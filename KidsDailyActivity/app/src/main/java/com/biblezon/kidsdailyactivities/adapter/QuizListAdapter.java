package com.biblezon.kidsdailyactivities.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.model.QuizBaseModel;
import com.biblezon.kidsdailyactivities.model.QuizOptionBaseModel;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.webservices.SubmitQuizAPIHandler;

import java.util.ArrayList;

/**
 * Created by sonia on 13/8/15.
 */
public class QuizListAdapter extends BaseAdapter {

    Context mContext;
    Activity activity;
    // ArrayList<NavItem> mNavItems;
    ArrayList<QuizBaseModel> listItems;
    String TAG = QuizListAdapter.class.getSimpleName();
    ArrayList<QuizOptionBaseModel> mOption = new ArrayList<>();

    public QuizListAdapter(Context context, ArrayList<QuizBaseModel> list) {
        mContext = context;
        activity = (Activity) context;
        listItems = new ArrayList<QuizBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public QuizBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<QuizBaseModel> updatedResponseModel) {
//        if (single) {
        if (updatedResponseModel != null && updatedResponseModel.size() > 0) {
            listItems = new ArrayList<QuizBaseModel>();
            listItems.add((updatedResponseModel.get(0)));
        }
//        } else {
//            listItems = new ArrayList<QuizBaseModel>();
//            listItems = (updatedResponseModel);
//        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(mContext.getApplicationContext(),
                    R.layout.quiz_list, null);
            new ViewHolder(convertView);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (listItems != null) {
            final QuizBaseModel responseBaseModel = listItems.get(position);
            holder.question.setText(responseBaseModel.getQuestion());
            holder.quiz_date.setText("Date : " + responseBaseModel.getDate());
            mOption = responseBaseModel.getOptions();

            /*Set Option GUI*/
            holder.mOptionTextFirst.setText(mOption.get(0).getOption());
            holder.mOptionTextSecond.setText(mOption.get(1).getOption());
            holder.mOptionTextThird.setText(mOption.get(2).getOption());
            holder.mOptionTextForth.setText(mOption.get(3).getOption());

             /*Unselect All*/
            holder.mSelectedFirst.setVisibility(View.GONE);
            holder.mSelectedSecond.setVisibility(View.GONE);
            holder.mSelectedThird.setVisibility(View.GONE);
            holder.mSelectedForth.setVisibility(View.GONE);
            holder.mUnSelectedFirst.setVisibility(View.VISIBLE);
            holder.mUnSelectedSecond.setVisibility(View.VISIBLE);
            holder.mUnSelectedThird.setVisibility(View.VISIBLE);
            holder.mUnSelectedForth.setVisibility(View.VISIBLE);

            if (responseBaseModel.isAlready_answered()) {
                if (mOption.get(0).getCorrect().equalsIgnoreCase("1"))
                    holder.mSelectedFirst.setVisibility(View.VISIBLE);
                else if (mOption.get(1).getCorrect().equalsIgnoreCase("1"))
                    holder.mSelectedSecond.setVisibility(View.VISIBLE);
                else if (mOption.get(2).getCorrect().equalsIgnoreCase("1"))
                    holder.mSelectedThird.setVisibility(View.VISIBLE);
                else if (mOption.get(3).getCorrect().equalsIgnoreCase("1"))
                    holder.mSelectedForth.setVisibility(View.VISIBLE);
            }

             /*First Option*/
            holder.mOptionFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listItems.get(position).isAlready_answered()) {
                        onOptionSelect(listItems.get(position).getOptions().get(0).getCorrect());
                        onSubmitAnswer(listItems.get(position).getOptions().get(0).getCorrect(), listItems.get(position).getOptions().get(0).getQuiz_id());
                    } else
                        AppDialogUtils.showAlertDialog(activity, "You have already answered this quiz.", "Ok", null);
                    listItems.get(position).setAlready_answered(true);
                }
            });
        /*Second Option*/
            holder.mOptionSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listItems.get(position).isAlready_answered()) {
                        onOptionSelect(listItems.get(position).getOptions().get(1).getCorrect());
                        onSubmitAnswer(listItems.get(position).getOptions().get(1).getCorrect(), listItems.get(position).getOptions().get(1).getQuiz_id());
                    } else
                        AppDialogUtils.showAlertDialog(activity, "You have already answered this quiz.", "Ok", null);
                    listItems.get(position).setAlready_answered(true);
                }
            });
         /*Third Option*/
            holder.mOptionThird.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listItems.get(position).isAlready_answered()) {
                        onOptionSelect(listItems.get(position).getOptions().get(2).getCorrect());
                        onSubmitAnswer(listItems.get(position).getOptions().get(2).getCorrect(), listItems.get(position).getOptions().get(2).getQuiz_id());
                    } else
                        AppDialogUtils.showAlertDialog(activity, "You have already answered this quiz.", "Ok", null);
                    listItems.get(position).setAlready_answered(true);

                }
            });
        /*Forth Option*/
            holder.mOptionForth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!listItems.get(position).isAlready_answered()) {
                        onOptionSelect(listItems.get(position).getOptions().get(3).getCorrect());
                        onSubmitAnswer(listItems.get(position).getOptions().get(3).getCorrect(), listItems.get(position).getOptions().get(3).getQuiz_id());
                    } else
                        AppDialogUtils.showAlertDialog(activity, "You have already answered this quiz.", "Ok", null);
                    listItems.get(position).setAlready_answered(true);
                }
            });

        }
        return convertView;
    }


    private void onOptionSelect(String correct) {
        if (correct.equalsIgnoreCase("0"))
            AppDialogUtils.showMessageInfoWithOkButtonDialog(activity, mContext.getResources().getString(R.string.wrong_answer),
                    mContext.getResources().getString(R.string.ohhh), R.color.red, R.color.black, null);
        else
            AppDialogUtils.showMessageInfoWithOkButtonDialog(activity, mContext.getResources().getString(R.string.right_answer),
                    mContext.getResources().getString(R.string.great), R.color.green, R.color.black, null);


    }

    private void onSubmitAnswer(String correct, String quiz_id) {
        new SubmitQuizAPIHandler(activity,
                quiz_id, correct,
                null);

        notifyDataSetChanged();
    }


    /**
     * List view row object and its views
     *
     * @author Shruti
     */
    class ViewHolder {
        TextView quiz_date, question;
        TextView mOptionTextFirst, mOptionTextSecond, mOptionTextThird, mOptionTextForth;
        ImageView mSelectedFirst, mSelectedSecond, mSelectedThird, mSelectedForth;
        ImageView mUnSelectedFirst, mUnSelectedSecond, mUnSelectedThird, mUnSelectedForth;
        LinearLayout mOptionFirst, mOptionSecond, mOptionThird, mOptionForth;

        public ViewHolder(View view) {
            quiz_date = (TextView) view
                    .findViewById(R.id.quiz_date);
            question = (TextView) view
                    .findViewById(R.id.question);
            /**
             * initialize option answer textviews
             */
            mOptionTextFirst = (TextView) view
                    .findViewById(R.id.first_option);
            mOptionTextSecond = (TextView) view
                    .findViewById(R.id.second_option);
            mOptionTextThird = (TextView) view
                    .findViewById(R.id.third_option);
            mOptionTextForth = (TextView) view
                    .findViewById(R.id.fourth_option);
            /**
             * initialize option answer selcted imageviews
             */
            mSelectedFirst = (ImageView) view
                    .findViewById(R.id.first_radio_selected);
            mSelectedSecond = (ImageView) view
                    .findViewById(R.id.second_radio_selected);
            mSelectedThird = (ImageView) view
                    .findViewById(R.id.third_radio_selected);
            mSelectedForth = (ImageView) view
                    .findViewById(R.id.fourth_radio_selected);

            /**
             * initialize option answer selcted imageviews
             */
            mUnSelectedFirst = (ImageView) view
                    .findViewById(R.id.first_radio_unselected);
            mUnSelectedSecond = (ImageView) view
                    .findViewById(R.id.second_radio_unselected);
            mUnSelectedThird = (ImageView) view
                    .findViewById(R.id.third_radio_unselected);
            mUnSelectedForth = (ImageView) view
                    .findViewById(R.id.fourth_radio_unselected);

            /**
             * initialize option answer selcted imageviews
             */
            mOptionFirst = (LinearLayout) view
                    .findViewById(R.id.first_option_ll);
//            mOptionFirst.setTag("0");
            mOptionSecond = (LinearLayout) view
                    .findViewById(R.id.second_option_ll);
//            mOptionSecond.setTag("1");
            mOptionThird = (LinearLayout) view
                    .findViewById(R.id.third_option_ll);
//            mOptionThird.setTag("2");
            mOptionForth = (LinearLayout) view
                    .findViewById(R.id.fourth_option_ll);
//            mOptionForth.setTag("3");
            view.setTag(this);
        }
    }
}
