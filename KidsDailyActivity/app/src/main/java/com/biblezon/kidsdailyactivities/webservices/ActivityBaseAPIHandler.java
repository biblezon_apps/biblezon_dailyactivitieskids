package com.biblezon.kidsdailyactivities.webservices;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.DrawingControl.ParseDrawApiResponse;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.biblezon.kidsdailyactivities.model.ActivityBaseModel;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;

/**
 * get fav drivers api Handler
 *
 * @author Shruti
 */
public class ActivityBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = ActivityBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<ActivityBaseModel> mMassBaseList = new ArrayList<ActivityBaseModel>();
    String MassUrl = "";
    int count = 0;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     * @param MassUrl
     */
    public ActivityBaseAPIHandler(Activity mActivity,
                                  WebAPIResponseListener webAPIResponseListener, String MassUrl) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        this.MassUrl = MassUrl;
        postAPICall(MassUrl);

    }

    /**
     * Making json object request
     *
     * @param massUrl
     */
    public void postAPICall(String massUrl) {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + massUrl).trim(), "",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        new ParseDrawApiResponse(mActivity, response, "");
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(
                jsonObjReq, GlobalKeys.MASS_ACTIVITY_API);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }
}
