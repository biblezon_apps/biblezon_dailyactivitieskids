package com.biblezon.kidsdailyactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.preference.SessionManager;

/**
 * Created by Shruti on 12/28/2015.
 */
public class SplashActivity extends Activity {

    RelativeLayout splash_rl;
    Animation zoom_in;
    Activity mActivity;
    TextView register, login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        initViews();
    }

    private void initViews() {
        mActivity = this;
        splash_rl = (RelativeLayout) findViewById(R.id.splash_rl);
        register = (TextView) findViewById(R.id.register);
        login = (TextView) findViewById(R.id.login);
        register.setVisibility(View.GONE);
        login.setVisibility(View.GONE);
        zoom_in = AnimationUtils.loadAnimation(mActivity, R.anim.zoom_in);
        splash_rl.startAnimation(zoom_in);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, SignUpActivity.class));
//                finish();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
//                finish();
            }
        });
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SessionManager.getInstance(mActivity).isLoggedIn()) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    register.setVisibility(View.VISIBLE);
                    login.setVisibility(View.VISIBLE);
                }
            }
        }, 3500);
    }
}
