package com.biblezon.kidsdailyactivities.model;

public class QuizOptionBaseModel {

    String quiz_id, option, correct, option_count;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getQuiz_id() {
        return quiz_id;
    }

    public void setQuiz_id(String quiz_id) {
        this.quiz_id = quiz_id;
    }

    public String getOption_count() {
        return option_count;
    }

    public void setOption_count(String option_count) {
        this.option_count = option_count;
    }
}
