package com.biblezon.kidsdailyactivities.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.DrawingControl.ParseDrawApiResponse;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class DrawActivitiesWebservice {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = DrawActivitiesWebservice.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public DrawActivitiesWebservice(Activity mActivity,
                                    WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                GlobalKeys.KIDS_DRAW_ACTIVITIES.trim(), "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                new ParseDrawApiResponse(mActivity, response, "");
                if (mResponseListener != null)
                    mResponseListener.onSuccessOfResponse();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showErrorLog(TAG, "Response Fail:"
                        + error);
                if (mResponseListener != null)
                    mResponseListener.onSuccessOfResponse();
            }
        }) {

        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(
                jsonObjReq, DrawActivitiesWebservice.class.getSimpleName());
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(1000
                * 20,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}
