package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Add quiz_id before ride API Handler
 *
 * @author Shruti
 */
public class SubmitQuizAPIHandler {
    /**
     * Instance object of Add quiz_id after ride API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = SubmitQuizAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private String quiz_id = "", is_correct = "0";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public SubmitQuizAPIHandler(Activity mActivity, String quiz_id, String is_correct,
                                WebAPIResponseListener webAPIResponseListener) {
//        AndroidAppUtils.showProgressDialog(mActivity, "Loading...",
//                false);
        this.mActivity = mActivity;
        this.quiz_id = quiz_id;
        this.is_correct = is_correct;
        this.mResponseListener = webAPIResponseListener;
        postAPICallString();

    }

    /**
     * Making String object request
     */
    public void postAPICallString() {
        String URL = (GlobalKeys.BASE_URL + GlobalKeys.SUBMIT_QUIZ).trim();
        AndroidAppUtils.showLog(TAG, "URL Post :" + URL);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL
                , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
//                AndroidAppUtils.hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showErrorLog(
                        TAG,
                        WebserviceAPIErrorHandler.getInstance()
                                .VolleyErrorHandlerReturningString(
                                        error, mActivity));
                if (mResponseListener != null)
                    mResponseListener.onFailOfResponse();
//                AndroidAppUtils.hideProgressDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.QUIZ_ID, quiz_id);
                params.put(GlobalKeys.IS_CORRECT, is_correct);
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getAuthToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(strReq, GlobalKeys.SUBMIT_QUIZ_KEY);
        // set request time-out
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalKeys.ONE_SECOND * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.CHANGE_KEY);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(String response) {
        // mResponseListener
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean status = WebserviceResponseHandler.getInstance()
                    .checkPrayerRequestResponseCode(jsonObject);
            if (status) {
            /* Response Success */
                if (mResponseListener != null)
                    mResponseListener.onSuccessOfResponse(response);

            } else {
            /* Response Status is null API Fail */
//                if (mResponseListener != null)
//                mResponseListener.onFailOfResponse();
//                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
//                        WebserviceResponseHandler.getInstance().getResponseMessage(jsonObject),
//                        "", R.color.black, R.color.orange, null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
