package com.biblezon.kidsdailyactivities.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.adapter.ReadingListAdapter;
import com.biblezon.kidsdailyactivities.control.HeaderViewManager;
import com.biblezon.kidsdailyactivities.iHelper.HeaderViewClickListener;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;
import com.biblezon.kidsdailyactivities.webservices.ReadingBaseAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class ReadingFragment extends Fragment implements DatePickerDialog.OnDateSetListener  {

    private String TAG = ReadingFragment.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    ListView reading_listview;
    private ReadingListAdapter mListAdapter;
    private ArrayList<ResponseBaseModel> mFilteredArrayList;
    boolean single = true;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.reading, container, false);
        initViews();
        manageHeaderOfScreen();
        MainActivity.getInstance().showProgressBar(true);
        new ReadingBaseAPIHandler(mActivity, ApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
        return mView;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        reading_listview = (ListView) mView.findViewById(R.id.reading_listview);
        reading_listview.setCacheColorHint(Color.TRANSPARENT);
        reading_listview.requestFocus(0);
        mFilteredArrayList = new ArrayList<>();
        mListAdapter = new ReadingListAdapter(mActivity,
                mFilteredArrayList);
        reading_listview.setAdapter(mListAdapter);
    }

    private void addDataIntoList(boolean singleorNot) {
        // TODO Auto-generated method stub
        mListAdapter.updateListData(mFilteredArrayList, singleorNot);
        mListAdapter.notifyDataSetChanged();
    }

    private WebAPIResponseListener ApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
                mFilteredArrayList = new ArrayList<>();
                if (arguments != null && arguments.length > 0) {
                    mFilteredArrayList = (ArrayList<ResponseBaseModel>) arguments[0];
                    if (mFilteredArrayList != null
                            && mFilteredArrayList.size() > 0) {
                        Collections.sort(mFilteredArrayList);
                        addDataIntoList(single);
                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
            }
        };
        return mListener;
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, true,
                manageHeaderClick());
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.todays_reading));
    }

    private HeaderViewClickListener manageHeaderClick() {
        HeaderViewClickListener headerViewClickListener = new HeaderViewClickListener() {
            @Override
            public void onClickOfHeaderLeftView() {

            }

            @Override
            public void onClickOfHeaderRightView() {
               /* if (single) {
                    single = false;
                    HeaderViewManager.getInstance().changeRightButtonText(single);
                    addDataIntoList(single);
                } else {
                    single = true;
                    HeaderViewManager.getInstance().changeRightButtonText(single);
                    addDataIntoList(single);
                }*/
                displayCalendar();
            }
        };
        return headerViewClickListener;
    }



    private void displayCalendar() {

        int year, month, dayOfMonth;


        Calendar now = Calendar.getInstance();
        year = now.get(Calendar.YEAR);
        month = now.get(Calendar.MONTH);
        dayOfMonth = now.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                year,
                month,
                dayOfMonth
        );
        // datePickerDialog.setThemeDark(true);
        datePickerDialog.setAccentColor("#f06e29");
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");

    }
    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String MonthDay = HeaderViewManager.getInstance().theMonth(month) + " " + dayOfMonth;
        String suffix=HeaderViewManager.getInstance().getDayOfMonthSuffix(dayOfMonth);

        month=month+1;
        String  ChosenDate=year +"-"+ month +"-"+ dayOfMonth;
        new ReadingBaseAPIHandler(mActivity, ApiResponseListener(),ChosenDate);
        HeaderViewManager.getInstance().SetButtonText(MonthDay, suffix);
        //  edtData.setText(SimpleDateFormat.getDateInstance().format(calendar.getTime()));
    }
}
