package com.biblezon.kidsdailyactivities.DrawingControl;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

import java.util.HashMap;


public class DrawMainDetailScreen extends Activity implements OnClickListener {

    private int sCurrentImageId = 0;

    // The saved state data
    private boolean isSavedState = false;
    private ColorGfxData savedData = null;
    private LinearLayout mColorPalette;
    // private LinearLayout mColorPalette2;

    // Define views.
    private FrameLayout mFlColorBody;

    // The render engine.
    public ColorGFX colorGFX;

    private MaskFilter mBlur;

    private ToggleButton mTbFillMode;
    private ToggleButton mTbEraseMode;
    public ProgressBar pbFloodFill;

    // Define a container for the palettes
    public HashMap<String, ColorPalette> hmPalette = new HashMap<String, ColorPalette>();

    // Tablet vs. phone boolean. Defaults to phone.
    public static boolean sIsTablet = false;
    public static boolean sIsSmall = false;
    public static boolean sIsNormal = false;
    public static boolean sIsLarge = false;
    public static boolean sIsExtraLarge = false;

    private String drawableImageResource;
    RelativeLayout header_icon;
    public static DrawMainDetailScreen mDetailPage;
    int key = 0;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawing_color);
        mDetailPage = this;
        key = getIntent().getExtras().getInt("key");
        header_icon = (RelativeLayout) findViewById(R.id.back_rl);
        header_icon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        TextView headerTxt = (TextView) findViewById(R.id.header_text);
        headerTxt.setText(ParseDrawApiResponse.mActivitiesModels.get(key)
                .getImageTitle());

        mFlColorBody = (FrameLayout) findViewById(R.id.flColorBody);
        mColorPalette = (LinearLayout) findViewById(R.id.llColorPalette);
        // mColorPalette2 = (LinearLayout) findViewById(R.id.llColorPalette2);

        mTbFillMode = (ToggleButton) findViewById(R.id.tbFillMode);
        mTbEraseMode = (ToggleButton) findViewById(R.id.tbEraseMode);


        // Determine whether or not the current device is a tablet.
        DrawMainDetailScreen.sIsTablet = getResources().getBoolean(R.bool.isTablet);
        DrawMainDetailScreen.sIsSmall = getResources().getBoolean(R.bool.isSmall);
        DrawMainDetailScreen.sIsNormal = getResources().getBoolean(R.bool.isNormal);
        DrawMainDetailScreen.sIsLarge = getResources().getBoolean(R.bool.isLarge);
        DrawMainDetailScreen.sIsExtraLarge = getResources().getBoolean(
                R.bool.isExtraLarge);

        final Object colorGfxData = getLastNonConfigurationInstance();

        if (colorGfxData != null) {

            isSavedState = true;
            savedData = ((ColorGfxData) colorGfxData);

            // Restore the previous data.
            sCurrentImageId = savedData.currentImageId;
        } else {
            isSavedState = false;
        }

        // Request focus to the FrameLayout containing the paint view.
        mFlColorBody.requestFocus();

        loadColorCanvas();

        // Load the color palettes.
        loadColorPalettes();

        loadPaletteButtons();

        loadBrushes();

        // set up listeners
        mTbFillMode.setOnClickListener(this);
        mTbEraseMode.setOnClickListener(this);
    }

    /*
     * Implements onRetainNonConfigurationInstance(). Saves current Gfx data for
     * resumes.
     */
    @Override
    public Object onRetainNonConfigurationInstance() {

        ColorGfxData colorGfxData = new ColorGfxData();

        colorGfxData.selectedColor = colorGFX.selectedColor;
        colorGfxData.isFillModeEnabled = colorGFX.isFillModeEnabled;
        colorGfxData.isEraseModeEnabled = colorGFX.isEraseModeEnabled;
        colorGfxData.bitmap = colorGFX.bitmap;
        colorGfxData.paint = colorGFX.paint;
        colorGfxData.currentImageId = sCurrentImageId;

        return colorGfxData;
    }

    /**
     * Implements onResume().
     */
    @Override
    protected void onResume() {
        super.onResume();

        // Resume the thread.
        colorGFX.resume();
    }

    /**
     * Implements onPause().
     */
    @Override
    protected void onPause() {
        super.onPause();

        // Pause the thread.
        colorGFX.pause();
    }

    /**
     * Sets up the coloring canvas. Loads the bitmap and draws it to the screen
     * on the canvas.
     */
    private void loadColorCanvas() {

        // Load the first image in the currently selected coloring book.
        loadImage();

        // Add a tag so we can reference it later. Usually ids gen'd in a loop.
        colorGFX.setTag(0);

        // Give the ImageButton some parameters.
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                colorGFX.imageWidth, colorGFX.imageHeight);
        params.gravity = Gravity.CENTER;
        colorGFX.setLayoutParams(params);

        // Add the image view to the layout.
        mFlColorBody.addView(colorGFX);
    }

    /**
     * Sets the image to display on the canvas.
     */
    private void loadImage() {

        // Ensure that the image is sized to fit to screen.
        Bitmap picture = decodeImage();

        // Instantiate the renderer if it doesn't yet exist.
        if (colorGFX == null) {
            colorGFX = new ColorGFX(DrawMainDetailScreen.this, picture.getWidth(),
                    picture.getHeight(), drawableImageResource);
        } else {
            // Clear the previous image and colors from the canvas.
            if (colorGFX.pathCanvas != null) {
                colorGFX.clear();
            }
        }

        // Clear the bitmaps from the screen.
        colorGFX.isNextImage = true;

        // Set the canvas's bitmap image so it can be drawn on canvas's run
        // method.
        colorGFX.pictureBitmapBuffer = picture;

    }

    /**
     * Resizes the image if it is too big for the screen. This should almost
     * never really be needed if the proper images are supplied to the drawable
     * folders. However, in practice this may not be the case and therefore,
     * this is used as a protection against these bad cases.
     */
    public Bitmap decodeImage() {
        String filePath = Environment.getExternalStorageDirectory()
                + "/"
                + GlobalKeys.STORE_IMAGE_FOLDER
                + "/"
                + ParseDrawApiResponse.mActivitiesModels.get(key)
                .getImageName() + ".png";
        drawableImageResource = ParseDrawApiResponse.mActivitiesModels.get(key)
                .getImageTitle();
        Log.d("DrawMainDetailScreen", "filePath : " + filePath);
        // Get the screen width and height.
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float screenWidth = dm.widthPixels;
        float screenHeight = dm.heightPixels;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        int inSampleSize = 1;
        int imageWidth = options.outWidth;
        int imageHeight = options.outHeight;

        // If the scale fails, we will need to use more memory to perform
        // scaling for the layout to work on all size screens.
        boolean scaleFailed = false;
        Bitmap scaledBitmap = null;
        float resizeRatioHeight = 1;

        // THIS IS DESIGNED FOR FITTING ON THE SCREEN WITH NO SCROLLBAR

        // Scale down if the image width exceeds the screen width.
        if (imageWidth > screenWidth || imageHeight > screenHeight) {

            // If we need to resize the image because the width or height is too
            // big, get the resize ratios for width and height.
            resizeRatioHeight = (float) imageHeight / (float) screenHeight;

            // Get the smaller ratio.
            inSampleSize = (int) resizeRatioHeight;

            if (inSampleSize <= 1) {
                scaleFailed = true;
            }
        }

        // Decode Bitmap with inSampleSize set
        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        Log.d("DrawMainDetailScreen", "scaleFailed : " + scaleFailed);
        Bitmap picture = BitmapFactory.decodeFile(filePath, options);

        // If the scale failed, that means a scale was needed but didn't happen.
        // We need to create a scaled copy of the image by allocating more
        // memory.
        if (scaleFailed) {
            int newWidth = (int) (picture.getWidth() / resizeRatioHeight);
            int newHeight = (int) (picture.getHeight() / resizeRatioHeight);

            scaledBitmap = Bitmap.createScaledBitmap(picture, newWidth,
                    newHeight, true);

            // Recycle the picture bitmap.
            picture.recycle();
        } else {
            // No scaling was needed in the first place!
            scaledBitmap = picture;
        }
        Log.d("DrawMainDetailScreen", "scaledBitmap : " + scaledBitmap);
        return scaledBitmap;
    }

    /**
     * Sets up the color palettes.
     */
    private void loadColorPalettes() {

		/*
         * Pallete 1
		 */

        // Create a tag and a HashMap of colors to assign to Palette1
        String tag = "Palette1";

        HashMap<String, Integer> colors = new HashMap<String, Integer>();
        colors.put("1_lightRed", Color.rgb(255, 106, 106));
        colors.put("2_red", Color.rgb(220, 20, 60));
        colors.put("3_orange", Color.rgb(255, 140, 0));
        colors.put("4_yellow", Color.rgb(255, 255, 0));
        colors.put("5_gold", Color.rgb(255, 185, 15));

        // Create a new palette based on this information.
        ColorPalette Palette1;

        if (isSavedState) {
            Palette1 = new ColorPalette(DrawMainDetailScreen.this, colors, isSavedState,
                    savedData.selectedColor);
        } else {
            Palette1 = new ColorPalette(DrawMainDetailScreen.this, colors);
        }

        // Add the palette to the HashMap.
        hmPalette.put(tag, Palette1);

		/*
         * Palette 2
		 */

        // Create a tag and a HashMap of colors to assign to Palette1
        tag = "Palette2";

        HashMap<String, Integer> colors2 = new HashMap<String, Integer>();
        colors2.put("1_green", Color.rgb(0, 205, 0));
        colors2.put("2_darkGreen", Color.rgb(0, 128, 0));
        colors2.put("3_lightBlue", Color.rgb(99, 184, 255));
        colors2.put("4_blue", Color.rgb(0, 0, 255));
        colors2.put("5_darkBlue", Color.rgb(39, 64, 139));

        // Create a new palette based on this information.
        ColorPalette Palette2;

        if (isSavedState) {
            Palette2 = new ColorPalette(this, colors2, isSavedState,
                    savedData.selectedColor);
        } else {
            Palette2 = new ColorPalette(this, colors2);
        }

        // Add the palette to the HashMap.
        hmPalette.put(tag, Palette2);

		/*
         * Palette 3
		 */

        // Create a tag and a HashMap of colors to assign to Palette1
        tag = "Palette3";

        HashMap<String, Integer> colors3 = new HashMap<String, Integer>();
        colors3.put("1_indigo", Color.rgb(75, 0, 130));
        colors3.put("2_violet", Color.rgb(148, 0, 211));
        colors3.put("3_pink", Color.rgb(255, 105, 180));
        colors3.put("4_peach", Color.rgb(255, 215, 164));
        colors3.put("5_lightBrown", Color.rgb(205, 133, 63));

        // Create a new palette based on this information.
        ColorPalette Palette3;

        if (isSavedState) {
            Palette3 = new ColorPalette(this, colors3, isSavedState,
                    savedData.selectedColor);
        } else {
            Palette3 = new ColorPalette(this, colors3);
        }

        // Add the palette to the HashMap.
        hmPalette.put(tag, Palette3);

		/*
         * Palette 4
		 */

        // Create a tag and a HashMap of colors to assign to Palette1
        tag = "Palette4";

        HashMap<String, Integer> colors4 = new HashMap<String, Integer>();
        colors4.put("1_black", Color.rgb(0, 0, 0));
        colors4.put("2_grey", Color.rgb(128, 128, 128));
        colors4.put("3_white", Color.rgb(255, 255, 255));
        colors4.put("4_lightgrey", Color.rgb(183, 183, 183));
        colors4.put("5_brown", Color.rgb(139, 69, 19));

        // Create a new palette based on this information.
        ColorPalette Palette4;

        if (isSavedState) {
            Palette4 = new ColorPalette(this, colors4, isSavedState,
                    savedData.selectedColor);
        } else {
            Palette4 = new ColorPalette(this, colors4);
        }

        // Add the palette to the HashMap.
        hmPalette.put(tag, Palette4);

    }

    /**
     * Loads the brush and it's stylings.
     */
    public void loadBrushes() {
        colorGFX.paint = new Paint();
        colorGFX.paint.setAntiAlias(true);
        colorGFX.paint.setDither(true);
        colorGFX.paint.setColor(colorGFX.selectedColor);
        colorGFX.paint.setStyle(Paint.Style.STROKE);
        colorGFX.paint.setStrokeJoin(Paint.Join.ROUND);
        colorGFX.paint.setStrokeCap(Paint.Cap.ROUND);
        colorGFX.paint.setStrokeWidth(12);
        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
        colorGFX.paint.setMaskFilter(mBlur);
    }

    /**
     * Creates Image Buttons for each color defined in each palette.
     */
    private void loadPaletteButtons() {
        // Iterate through the palettes
        for (String key : hmPalette.keySet()) {
            // Load the button size.
            hmPalette.get(key).calculateButtonSize();
            // Get the palette object and create ImageButtons for the color set
            // that corresponds to that palette object.
            hmPalette.get(key).createButtons();
            // Add the palettes to a view.
        }

        // Set the left Palette buttons on the screen.
        hmPalette.get("Palette1").addToView(mColorPalette);

        // Set the left Palette buttons on the screen.
        hmPalette.get("Palette2").addToView(mColorPalette);

        // Set the right Palette buttons on the screen.
        // hmPalette.get("Palette3").addToView(mColorPalette2);

        // Set the right Palette buttons on the screen.
        // hmPalette.get("Palette4").addToView(mColorPalette2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tbFillMode:

                // Check to see if erase mode is enabled.
                if (mTbEraseMode.isChecked()) {
                    // If it is, simply set this button as the enabled button.

                    // Prevent toggle.
                    mTbFillMode.setChecked(!mTbFillMode.isChecked());
                    colorGFX.isFillModeEnabled = mTbFillMode.isChecked();

                    // Disable erase mode
                    colorGFX.paint.setXfermode(null);
                    // Set the blur mode on again for path drawing.
                    colorGFX.paint.setMaskFilter(mBlur);
                    // Set the isEraseModeEnabled boolean
                    colorGFX.isEraseModeEnabled = false;

                    // Turn the eraser button off.
                    mTbEraseMode.setChecked(false);

                    // Replace the drawable with the color versions.
                    mTbFillMode.setBackgroundResource(R.drawable.bucket_button);
                } else {
                    colorGFX.isFillModeEnabled = mTbFillMode.isChecked();
                }

                break;

            case R.id.tbEraseMode:

                boolean isEraseModeEnabled = mTbEraseMode.isChecked();

                if (isEraseModeEnabled) {

                    // Set the disabled image resources for the brush and fill
                    // buttons.
                    mTbFillMode
                            .setBackgroundResource(R.drawable.bucket_button_disabled);

                    // Set the current brush mode to erase.
                    // colorGFX.paint.setAlpha(0xFF);//transperent color
                    colorGFX.paint.setXfermode(new PorterDuffXfermode(
                            PorterDuff.Mode.LIGHTEN));
                    colorGFX.paint.setColor(0Xffffffff);

                    // // Take the blur mode off for the eraser.
                    colorGFX.paint.setMaskFilter(null);
                    // Set the colorGFX isEraseModeEnabled Boolean
                    colorGFX.isEraseModeEnabled = true;
                } else {
                    // Set the enabled image resources for the brush and fill
                    // buttons.
                    mTbFillMode.setBackgroundResource(R.drawable.bucket_button);

                    colorGFX.paint.setXfermode(null);
                    // Set the blur mode on again for path drawing.
                    colorGFX.paint.setMaskFilter(mBlur);
                    // Set the isEraseModeEnabled boolean
                    colorGFX.isEraseModeEnabled = false;
                }

                break;

//            case R.id.chk_icon:
//                takeScreenShot();
//                Toast.makeText(DrawMainDetailScreen.this, "Save image successfully",
//                        Toast.LENGTH_SHORT).show();
//                break;
        }
    }

    @SuppressWarnings("static-access")
    private void takeScreenShot() {

//        try {
//            Bitmap bitmap = Bitmap.createBitmap(colorGFX.getWidth(),
//                    colorGFX.getHeight(), Bitmap.Config.ARGB_8888);
//            Canvas canvas = new Canvas(bitmap);
//            canvas.drawColor(Color.WHITE);
//            canvas.drawBitmap(colorGFX.bitmap, 0f, 0f, null);
//            canvas.drawBitmap(colorGFX.pictureBitmap, 0f, 0f, null);
//            File folder = new File(Environment.getExternalStorageDirectory()
//                    + "/" + GlobalKeys.MY_DRAW_FOLDER);
//            if (!folder.exists()) {
//                folder.mkdir();
//            }
//            File imagePath = new File(Environment.getExternalStorageDirectory()
//                    + "/" + GlobalKeys.MY_DRAW_FOLDER + "/kids_"
//                    + System.currentTimeMillis() + ".jpg");
//            FileOutputStream fos;
//            try {
//                fos = new FileOutputStream(imagePath);
//                bitmap.compress(CompressFormat.JPEG, 100, fos);
//                fos.flush();
//                fos.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    // private void takeScreenshot() {
    // Date now = new Date(sCurrentImageId);
    // android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);
    //
    // try {
    // // image naming and path to include sd card appending name you
    // // choose for file
    // String mPath = Environment.getExternalStorageDirectory().toString()
    // + "/" + now + ".jpg";
    //
    // // create bitmap screen capture
    // View v1 = getWindow().getDecorView().getRootView();
    // v1.setDrawingCacheEnabled(true);
    // Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
    // v1.setDrawingCacheEnabled(false);
    //
    // File imageFile = new File(mPath);
    //
    // FileOutputStream outputStream = new FileOutputStream(imageFile);
    // int quality = 100;
    // bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
    // outputStream.flush();
    // outputStream.close();
    //
    // // openScreenshot(imageFile);
    // } catch (Throwable e) {
    // // Several error may come out with file handling or OOM
    // e.printStackTrace();
    // }
    // }

}
