package com.biblezon.kidsdailyactivities.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.adapter.QuizListAdapter;
import com.biblezon.kidsdailyactivities.control.HeaderViewManager;
import com.biblezon.kidsdailyactivities.iHelper.ChoiceDialogClickListener;
import com.biblezon.kidsdailyactivities.iHelper.HeaderViewClickListener;
import com.biblezon.kidsdailyactivities.model.QuizBaseModel;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.webservices.QuizBaseAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class QuizOfDayFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private static ArrayList<QuizBaseModel> mFilteredArrayList = new ArrayList<>();
    ListView quiz_listview;
    boolean single = true;
    private String TAG = QuizOfDayFragment.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    private QuizListAdapter mListAdapter;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.quiz_of_day, container, false);
        initViews();
        manageHeaderOfScreen();
        HeaderViewManager.mHeaderManagerInstance.changeRightButtonText(true);
        MainActivity.getInstance().showProgressBar(true);
        new QuizBaseAPIHandler(mActivity, ApiResponseListener(), HeaderViewManager.getInstance().getChosenDate());
        return mView;
    }

    private void checkAndShowingRatingOfPlayer() {
        String ratingDate = SessionManager.getInstance(mActivity).getRatingDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date()); // Now use today date.
        String todaysDate = sdf.format(c.getTime());
        AndroidAppUtils.showInfoLog(TAG, "Rating Date is :" + ratingDate);
        AndroidAppUtils.showInfoLog(TAG, "Todays Date is :" + todaysDate);
        if (ratingDate.equalsIgnoreCase(todaysDate)) {
            /*Show Dialog*/


            /*Update New
            ratingDate*/

            c.setTime(new Date()); // Now use today date.
            c.add(Calendar.DATE, 5); // Adding 5 days
            String output = sdf.format(c.getTime());
            SessionManager.getInstance(mActivity).setRatingDate(output);
            AndroidAppUtils.showInfoLog(TAG, "Next Upcoming rating date is :" + output);
        }
    }

    private void addDataIntoList(ArrayList<QuizBaseModel> mTemp) {
        mListAdapter.updateListData(mTemp);
        mListAdapter.notifyDataSetChanged();
    }

    private WebAPIResponseListener ApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
                if (arguments != null && arguments.length > 0) {
                    ArrayList<QuizBaseModel> mTemp = (ArrayList<QuizBaseModel>) arguments[0];
                    addDataIntoList(mTemp);

//                    for (int z = 0; z < mTemp.size(); z++) {
//                        boolean isSkip = false;
//                        for (int u = 0; u < mFilteredArrayList.size(); u++) {
//                            if (mFilteredArrayList.get(u).getId().equalsIgnoreCase(mTemp.get(z).getId())) {
//                                isSkip = true;
//                            }
//                        }
//                        if (!isSkip) {
//                            AndroidAppUtils.showVerboseLog(TAG, "Adding New Data Into quiz.");
//                            mFilteredArrayList.add(mTemp.get(z));
//                        }
//                    }
                    if (mFilteredArrayList != null
                            && mFilteredArrayList.size() > 0) {
                        Collections.sort(mFilteredArrayList);
//                        addDataIntoList();

                        int score = mFilteredArrayList.get(0).getScore();
                        boolean showscore = mFilteredArrayList.get(0).isShowscore();
                        if (showscore) {
                            String previousTrueDate = SessionManager.getInstance(mActivity).getRatingDate();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar c = Calendar.getInstance();
                            c.setTime(new Date()); // Now use today date.
                            String todaysDate = sdf.format(c.getTime());

                            AndroidAppUtils.showLog(TAG, "previous : " + previousTrueDate + " \n today : " + todaysDate);
                            if (previousTrueDate == null || !previousTrueDate.equalsIgnoreCase(todaysDate)) {
                                SessionManager.getInstance(mActivity).setRatingDate(todaysDate);
                                showRatingDialog(score);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
            }
        };
        return mListener;
    }


    private HeaderViewClickListener manageHeaderClick() {
        HeaderViewClickListener headerViewClickListener = new HeaderViewClickListener() {
            @Override
            public void onClickOfHeaderLeftView() {

            }

            @Override
            public void onClickOfHeaderRightView() {
                /*if (single) {
                    single = false;
                    HeaderViewManager.getInstance().changeRightButtonText(single);
                    addDataIntoList(single);
                } else {
                    single = true;
                    HeaderViewManager.getInstance().changeRightButtonText(single);
                    addDataIntoList(single);
                }*/
                displayCalendar();
            }
        };
        return headerViewClickListener;
    }

    /**
     * Multiple User choice dialog
     *
     * @param mActivity
     * @param msg
     * @param positiveButtonText
     * @param navgitaveButtonText
     */
    public static void showChoiceDialog(Activity mActivity, String msg,
                                        String positiveButtonText, String navgitaveButtonText,
                                        final ChoiceDialogClickListener mChoiceDialogClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton(positiveButtonText,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mChoiceDialogClickListener.onClickOfPositive();
                    }
                });
        alertDialogBuilder.setNegativeButton(navgitaveButtonText,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mChoiceDialogClickListener.onClickOfNegative();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Show Message Info View
     */
    @SuppressLint({"InflateParams", "DefaultLocale"})
    public void showRatingDialog(int score) {
        /* Check if Alert is already there so remove old one */
        try {
            // Create custom dialog object
            final Dialog dialog = new Dialog(mActivity);
            // hide to default title for Dialog
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            // inflate the layout dialog_layout.xml and set it as
            // contentView
            LayoutInflater inflater = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(
                    R.layout.rating_popup, null, false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setContentView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

            // Retrieve views from the inflated dialog layout and update
            // their
            // values
            RatingBar rating_Bar = (RatingBar) dialog.findViewById(R.id.rating_Bar);
            rating_Bar.setRating(score);
            TextView messageText = (TextView) dialog
                    .findViewById(R.id.msgText);
            switch (score) {
                case 0:
                    messageText.setText(mActivity.getResources().getString(R.string.poor));
                    break;
                case 1:
                    messageText.setText(mActivity.getResources().getString(R.string.poor));
                    break;
                case 2:
                    messageText.setText(mActivity.getResources().getString(R.string.poor));
                    break;
                case 3:
                    messageText.setText(mActivity.getResources().getString(R.string.good));
                    break;
                case 4:
                    messageText.setText(mActivity.getResources().getString(R.string.very_good));
                    break;
                case 5:
                    messageText.setText(mActivity.getResources().getString(R.string.excellent));
                    break;

            }
            ImageView close_btn = (ImageView) dialog
                    .findViewById(R.id.close_btn);
            close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Dismiss the dialog
                    dialog.dismiss();
                }
            });
            try {
                // Display the dialog
                dialog.show();
            } catch (WindowManager.BadTokenException e) {
                // TODO: handle exception
                e.printStackTrace();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        quiz_listview = (ListView) mView.findViewById(R.id.quiz_listview);
        quiz_listview.setCacheColorHint(Color.TRANSPARENT);
        quiz_listview.requestFocus(0);
        mListAdapter = new QuizListAdapter(mActivity,
                mFilteredArrayList);
        quiz_listview.setAdapter(mListAdapter);
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, true,
                manageHeaderClick());
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.quiz_of_the_day));
    }


    private void displayCalendar() {

        int year, month, dayOfMonth;


        Calendar now = Calendar.getInstance();
        year = now.get(Calendar.YEAR);
        month = now.get(Calendar.MONTH);
        dayOfMonth = now.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                year,
                month,
                dayOfMonth
        );
        // datePickerDialog.setThemeDark(true);
        datePickerDialog.setAccentColor("#f06e29");
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String MonthDay = HeaderViewManager.getInstance().theMonth(month) + " " + dayOfMonth;
        String suffix = HeaderViewManager.getInstance().getDayOfMonthSuffix(dayOfMonth);

        month = month + 1;
        String ChosenDate = year + "-" + month + "-" + dayOfMonth;
        new QuizBaseAPIHandler(mActivity, ApiResponseListener(), ChosenDate);
        HeaderViewManager.getInstance().SetButtonText(MonthDay, suffix);
        //  edtData.setText(SimpleDateFormat.getDateInstance().format(calendar.getTime()));
    }
}
