package com.biblezon.kidsdailyactivities.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;

import java.util.ArrayList;

/**
 * Created by sonia on 13/8/15.
 */
public class AnnounceListAdapter extends BaseAdapter {

    Context mContext;
    // ArrayList<NavItem> mNavItems;
    ArrayList<ResponseBaseModel> listItems;
    String TAG = AnnounceListAdapter.class.getSimpleName();

    public AnnounceListAdapter(Context context, ArrayList<ResponseBaseModel> list) {
        mContext = context;
        listItems = new ArrayList<ResponseBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public ResponseBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<ResponseBaseModel> updatedResponseModel, boolean single) {
        if (single) {
            if (updatedResponseModel != null && updatedResponseModel.size() > 0) {
                listItems = new ArrayList<ResponseBaseModel>();
                listItems.add(updatedResponseModel.get(0));
            }
        } else {
            listItems = new ArrayList<ResponseBaseModel>();
            listItems.addAll(updatedResponseModel);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_row, null);
        } else {
            view = convertView;
        }
        ResponseBaseModel responseBaseModel = listItems.get(position);
        if (responseBaseModel != null) {
            TextView titleView = (TextView) view.findViewById(R.id.tv_title);
            TextView dateView = (TextView) view
                    .findViewById(R.id.date);
            titleView.setText((Html.fromHtml(responseBaseModel.getDescription()).toString()));
            dateView.setText("Date : " + responseBaseModel.getDate());
            // serialview.setText(responseBaseModel.getActivityID() + ".)");
        }
        return view;
    }
}
