package com.biblezon.kidsdailyactivities.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;

import com.biblezon.kidsdailyactivities.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class AppUtils {


    static String TAG = AppUtils.class.getSimpleName();

    /**
     * Saved Image Path
     *
     * @param finalBitmap
     * @param image
     */
    public static void savePassengerBitmapToSDCard(Bitmap finalBitmap,
                                                   String image) {
        String targetFileName;
        if (image.contains(".png")) {
            targetFileName = image.replace(".png", "");
        } else if (image.contains(".jpg")) {
            targetFileName = image.replace(".jpg", "");
        } else {
            targetFileName = image;
        }
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/" + GlobalKeys.KidsDailyActivities);
        myDir.mkdirs();
        String fname = targetFileName;
        File file = new File(myDir, fname + ".png");
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileNameFromUrl(String url) {
        if (url != null && !url.isEmpty()) {
            String fileName = "";
            if (url.contains(".png")) {
                fileName = (url.substring(url.lastIndexOf('/') + 1,
                        url.lastIndexOf(".")))
                        + ".png";
            } else if (url.contains(".jpg")) {
                fileName = (url.substring(url.lastIndexOf('/') + 1,
                        url.lastIndexOf(".")))
                        + ".jpg";
            }
            return fileName;
        } else {
            return "";
        }
    }

    public static boolean isFilePresent(String fileName) {
        String root = Environment.getExternalStorageDirectory().toString();
        String fileStoragePath = root + "/" + GlobalKeys.KidsDailyActivities;
        String path = fileStoragePath + "/" + fileName + ".png";
        File file = new File(path);
        return file.exists();
    }

    /**
     * checks if file already exists in the SD card
     *
     * @param image_name image name
     * @return
     */
    public static boolean checkIfImageAlreadyExists(String image_name) {

        if (image_name.contains(".png")) {
            image_name = image_name.replace(".png", "");
        } else if (image_name.contains(".jpg")) {
            image_name = image_name.replace(".jpg", "");
        }

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/" + GlobalKeys.KidsDailyActivities);
        if (!myDir.exists()) {
            // myDir.mkdirs();
            return false;
        }
        String fname = image_name + ".png";

        AndroidAppUtils.showLog(TAG, "fname : " + fname);
        File file = new File(myDir, fname);
        if (file.exists())
            return true;
        else
            return false;
    }

    public static Bitmap getImageFromSDCard(String image_name) {
        /**
         * get the User image, capitalize it and show on UI
         */
        if (image_name.contains(".png")) {
            image_name = image_name.replace(".png", "");
        } else if (image_name.contains(".jpg")) {
            image_name = image_name.replace(".jpg", "");
        }
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/" + GlobalKeys.KidsDailyActivities);
        String fname = image_name + ".png";
        File file = new File(myDir, fname);
        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
        return bmp;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    public static void copyImageFromResourceToSDCard(String image_name, Activity mActivity) {
        String targetFileName;
        if (image_name.contains(".png")) {
            targetFileName = image_name.replace(".png", "");
        } else if (image_name.contains(".jpg")) {
            targetFileName = image_name.replace(".jpg", "");
        } else {
            targetFileName = image_name;
        }
        Bitmap bm = BitmapFactory.decodeResource(mActivity.getResources(),
                AndroidAppUtils.getId(targetFileName.toLowerCase(), R.drawable.class));
        if (bm != null) {
            String PATH = Environment.getExternalStorageDirectory() + "/"
                    + GlobalKeys.KidsDailyActivities + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();// If there is no folder it will be created.
            }
            File file = new File(PATH, image_name);
            FileOutputStream outStream;
            try {
                outStream = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static String LoadData(Activity activity, String inFile) {
        String tContents = "";

        try {
            InputStream stream = activity.getAssets().open(inFile);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
            e.printStackTrace();
        }

        return tContents;

    }
}
