package com.biblezon.kidsdailyactivities.iHelper;

/**
 * This interface to manage Multiple choice dialog click
 *
 * @author Anshuman
 */
public interface ChoiceDialogClickListener {
    /* Listener Left side Click */
    void onClickOfPositive();

    /* Listener Right side Click */
    void onClickOfNegative();

}
