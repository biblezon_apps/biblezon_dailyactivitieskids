package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Add password before ride API Handler
 *
 * @author Shruti
 */
public class LoginAPIHandler {
    /**
     * Instance object of Add password after ride API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = LoginAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private String password = "", email = "";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public LoginAPIHandler(Activity mActivity, String email, String password,
                           WebAPIResponseListener webAPIResponseListener) {
        AndroidAppUtils.showProgressDialog(mActivity, "Loading...",
                false);
        this.mActivity = mActivity;
        this.password = password;
        this.email = email;
        AndroidAppUtils.showLog(TAG, "password :" + password + "\n email : " + email);
        this.mResponseListener = webAPIResponseListener;
        postAPICallString();
    }

    /**
     * Making String object request
     */
    public void postAPICallString() {
        String URL = (GlobalKeys.BASE_URL + GlobalKeys.KIDS_LOGIN).trim();
        AndroidAppUtils.showLog(TAG, "URL Post :" + URL);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL
                , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
                AndroidAppUtils.hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showErrorLog(
                        TAG,
                        WebserviceAPIErrorHandler.getInstance()
                                .VolleyErrorHandlerReturningString(
                                        error, mActivity));
                mResponseListener.onFailOfResponse();
                AndroidAppUtils.hideProgressDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.PRAYER_REQUEST_PASSWORD, password);
                params.put(GlobalKeys.PRAYER_REQUEST_EMAIL, email);
                return params;
            }
        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(strReq, GlobalKeys.LOGIN_KEY);
        // set request time-out
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalKeys.ONE_SECOND * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.CHANGE_KEY);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(String response) {
        // mResponseListener
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean status = WebserviceResponseHandler.getInstance()
                    .checkPrayerRequestResponseCode(jsonObject);
            if (status) {
            /* Response Success */
                AndroidAppUtils.showLog(TAG, "token : " + jsonObject.getString(GlobalKeys.TOKEN));
                mResponseListener.onSuccessOfResponse(jsonObject.getString(GlobalKeys.TOKEN));

            } else {
            /* Response Status is null API Fail */
                mResponseListener.onFailOfResponse();
                AppDialogUtils.showAlertDialog(mActivity, WebserviceResponseHandler.getInstance().getResponseMessage(jsonObject),
                        "Ok", null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
