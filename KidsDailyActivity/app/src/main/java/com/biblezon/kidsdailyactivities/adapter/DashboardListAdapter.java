package com.biblezon.kidsdailyactivities.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.control.LeftSlidingMenuControl;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

import java.util.ArrayList;

public class DashboardListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<ResponseBaseModel> listItems;
    String TAG = DashboardListAdapter.class.getSimpleName();

    public DashboardListAdapter(Context context, ArrayList<ResponseBaseModel> list) {
        mContext = context;
        listItems = new ArrayList<ResponseBaseModel>();
    }

    @Override
    public int getCount() {
        if (listItems != null) {
//			AndroidAppUtils.showLog(TAG,
//					"listItems.size() : " + listItems.size());
        }
        return listItems.size();
    }

    @Override
    public ResponseBaseModel getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void updateListData(ArrayList<ResponseBaseModel> updatedResponseModel) {
        if (updatedResponseModel != null && updatedResponseModel.size() > 0) {
            listItems = new ArrayList<ResponseBaseModel>();
            listItems.addAll(updatedResponseModel);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dashboard_list_row, null);
        } else {
            view = convertView;
        }
        final ResponseBaseModel responseBaseModel = listItems.get(position);
        if (responseBaseModel != null) {
            TextView titleView = (TextView) view.findViewById(R.id.title);
            TextView description = (TextView) view.findViewById(R.id.description);
            titleView.setText(responseBaseModel.getId());
            description.setText(responseBaseModel.getTitle());
            titleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showNewFragment(responseBaseModel.getId());
                }
            });

            description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showNewFragment(responseBaseModel.getId());
                }
            });

            // serialview.setText(responseBaseModel.getActivityID() + ".)");
        }
        return view;
    }

    private void showNewFragment(String id) {
        if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_REFLECTION)) {
//            MainActivity.getInstance().pushFragment(new ReflectionFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(2);
        } else if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_READING)) {
//            MainActivity.getInstance().pushFragment(new ReadingFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(1);
        } else if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_SAINT_OF_DAY)) {
//            MainActivity.getInstance().pushFragment(new SaintOfDayFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(3);
        } else if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_QUIZ_OF_DAY)) {
//            MainActivity.getInstance().pushFragment(new QuizOfDayFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(4);
        } else if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_WORD_OF_DAY)) {
//            MainActivity.getInstance().pushFragment(new PrayerRequestFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(5);
        } else if (id.equalsIgnoreCase(GlobalKeys.DASHBOARD_TOPICS)) {
//            MainActivity.getInstance().pushFragment(new DiscussionFragment());
            LeftSlidingMenuControl.getInstance().callNextFragment(7);
        }
    }

}
