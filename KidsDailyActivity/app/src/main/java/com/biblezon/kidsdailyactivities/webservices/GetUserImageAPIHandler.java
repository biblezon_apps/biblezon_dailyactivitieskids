package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.graphics.Bitmap;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

/**
 * Get user image API Handler
 *
 * @author Shruti
 */
public class GetUserImageAPIHandler {
    /**
     * Instance object of get image API
     */
    @SuppressWarnings("unused")
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetUserImageAPIHandler.class.getSimpleName();
    /**
     * Request Data
     */
    private String image_path;
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    int position;

    /**
     * @param mActivity
     */
    public GetUserImageAPIHandler(Activity mActivity, String image_path,
                                  WebAPIResponseListener webAPIResponseListener,
                                  boolean isProgreesShowing, int position) {
//        if (isProgreesShowing) {
//            AndroidAppUtils.showProgressDialog(mActivity, "Loading...", false);
//        }

        this.mActivity = mActivity;
        this.image_path = image_path;
        this.mResponseListener = webAPIResponseListener;
        this.position = position;
        postAPICall();
    }

    /**
     * Call API to Get User image
     */
    private void postAPICall() {
        ImageRequest mImageRequest = new ImageRequest(image_path.trim(),
                new Listener<Bitmap>() {

                    @Override
                    public void onResponse(Bitmap bitmap) {
                        if (bitmap != null) {
                            AndroidAppUtils.showLog(TAG,
                                    "Success To get user image");
                            mResponseListener.onSuccessOfResponse(bitmap);
                        } else {
                            AndroidAppUtils.showLog(TAG, "bitmap is null");
                            // mResponseListener.onFailOfResponse(mActivity
                            // .getResources().getString(R.string.RETRY));
                            mResponseListener.onFailOfResponse("");
                        }
                    }
                }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showLog(TAG,
                        "get image onErrorResponse " + error);
                mResponseListener.onFailOfResponse("");
            }
        }) {

        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToImageRequestQueue(
                mImageRequest, GlobalKeys.GET_IMAGE_KEY + position);
        // set request time-out
        mImageRequest.setRetryPolicy(new DefaultRetryPolicy(
                GlobalKeys.ONE_SECOND
                        * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ReDriverApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.LOGIN_REQUEST_KEY);
    }
}