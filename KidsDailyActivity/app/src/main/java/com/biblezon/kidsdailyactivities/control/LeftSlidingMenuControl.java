package com.biblezon.kidsdailyactivities.control;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.biblezon.kidsdailyactivities.DrawingControl.DrawingLoadingScreen;
import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.fragments.AnnounceFragment;
import com.biblezon.kidsdailyactivities.fragments.DashboardFragment;
import com.biblezon.kidsdailyactivities.fragments.PrayerFragment;
import com.biblezon.kidsdailyactivities.fragments.QuizOfDayFragment;
import com.biblezon.kidsdailyactivities.fragments.ReadingFragment;
import com.biblezon.kidsdailyactivities.fragments.ReflectionFragment;
import com.biblezon.kidsdailyactivities.fragments.SaintOfDayFragment;
import com.biblezon.kidsdailyactivities.fragments.TopicsFragment;
import com.biblezon.kidsdailyactivities.fragments.WordOfDayFragment;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Manage Left Side Sliding Menu
 *
 * @author Anshuman
 */
public class LeftSlidingMenuControl implements OnClickListener {
    private static Activity mActivity;
    /**
     * Manage Menu Slider layout view object
     */

//    private RelativeLayout userProfileView, settingIcon;
    //    private LinearLayout announceView, deviceView, occView, contactUsView,
//            helpView, logoutView, storeLocatorView;
    private RelativeLayout selectedViews[] = new RelativeLayout[9];
    private RelativeLayout unselectedViews[] = new RelativeLayout[9];
    private ImageView images[] = new ImageView[9];
    /**
     * Currently Selected TAG
     */
    private int currentSelectedTAG = 0;

    /**
     * Debugging TAG
     */
    @SuppressWarnings("unused")
    private String TAG = LeftSlidingMenuControl.class.getSimpleName();
    public static LeftSlidingMenuControl mLeftSlidingMenuControl;

    /**
     * Left Side Sliding Menu Control constructor
     */
    public LeftSlidingMenuControl(Activity mAct) {
        mActivity = mAct;
        mLeftSlidingMenuControl = this;
        initSlidingView();
        assignClicks();
        assignTags();
        CheckWhichTabIsToBlink();
    }

    private void CheckWhichTabIsToBlink() {
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getHomeDate())) {
            animateText(images[0], true);
        } else {
            animateText(images[0], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getTodaysReadingDate())) {
            animateText(images[1], true);
        } else {
            animateText(images[1], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getReflectionDate())) {
            animateText(images[2], true);
        } else {
            animateText(images[2], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getSaintOFDayDate())) {
            animateText(images[3], true);
        } else {
            animateText(images[3], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getQuizOFDayDate())) {
            animateText(images[4], true);
        } else {
            animateText(images[4], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getWordOFDayDate())) {
            animateText(images[5], true);
        } else {
            animateText(images[5], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getActivitiesDate())) {
            animateText(images[6], true);
        } else {
            animateText(images[6], false);
        }

        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getTopicsDate())) {
            animateText(images[7], true);
        } else {
            animateText(images[7], false);
        }
        if (AndroidAppUtils.checkClickDate(SessionManager.getInstance(mActivity).getPrayerDate())) {
            animateText(images[8], true);
        } else {
            animateText(images[8], false);
        }
    }

    private void animateText(ImageView text_to_blink, boolean animate) {
        if (animate) {
            text_to_blink.setVisibility(View.VISIBLE);
        } else {
            text_to_blink.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Instance of this class
     *
     * @return
     */
    public static LeftSlidingMenuControl getInstance() {

        return mLeftSlidingMenuControl;
    }

    /**
     * Manage Sliding View object
     */
    private void initSlidingView() {

        /*
        initialize selected linear layouts
         */
        selectedViews[0] = (RelativeLayout) mActivity.findViewById(R.id.announceViewSelected);
        selectedViews[1] = (RelativeLayout) mActivity.findViewById(R.id.readingViewSelected);
        selectedViews[2] = (RelativeLayout) mActivity.findViewById(R.id.reflectionViewSelected);
        selectedViews[3] = (RelativeLayout) mActivity.findViewById(R.id.saintViewSelected);
        selectedViews[4] = (RelativeLayout) mActivity.findViewById(R.id.quizViewSelected);
        selectedViews[5] = (RelativeLayout) mActivity.findViewById(R.id.wordViewSelected);
        selectedViews[6] = (RelativeLayout) mActivity.findViewById(R.id.blogsViewSelected);
        selectedViews[7] = (RelativeLayout) mActivity.findViewById(R.id.forumsViewSelected);
        selectedViews[8] = (RelativeLayout) mActivity.findViewById(R.id.prayerViewSelected);
         /*
        initialize unselected linear layouts
         */
        unselectedViews[0] = (RelativeLayout) mActivity.findViewById(R.id.announceView);
        unselectedViews[1] = (RelativeLayout) mActivity.findViewById(R.id.readingView);
        unselectedViews[2] = (RelativeLayout) mActivity.findViewById(R.id.reflectionView);
        unselectedViews[3] = (RelativeLayout) mActivity.findViewById(R.id.saintView);
        unselectedViews[4] = (RelativeLayout) mActivity.findViewById(R.id.quizView);
        unselectedViews[5] = (RelativeLayout) mActivity.findViewById(R.id.wordView);
        unselectedViews[6] = (RelativeLayout) mActivity.findViewById(R.id.blogsView);
        unselectedViews[7] = (RelativeLayout) mActivity.findViewById(R.id.forumsView);
        unselectedViews[8] = (RelativeLayout) mActivity.findViewById(R.id.prayerView);
        /**
         * initialize image view
         */
        images[0] = (ImageView) mActivity.findViewById(R.id.imageview_s1);
        images[1] = (ImageView) mActivity.findViewById(R.id.imageview_s2);
        images[2] = (ImageView) mActivity.findViewById(R.id.imageview_s3);
        images[3] = (ImageView) mActivity.findViewById(R.id.imageview_s4);
        images[4] = (ImageView) mActivity.findViewById(R.id.imageview_s5);
        images[5] = (ImageView) mActivity.findViewById(R.id.imageview_s6);
        images[6] = (ImageView) mActivity.findViewById(R.id.imageview_s7);
        images[7] = (ImageView) mActivity.findViewById(R.id.imageview_s8);
        images[8] = (ImageView) mActivity.findViewById(R.id.imageview_s9);

    }

    /**
     * Assign click to View objects
     */
    private void assignClicks() {
        for (int i = 0; i < selectedViews.length; i++) {
            unselectedViews[i].setOnClickListener(this);
            unselectedViews[i].setVisibility(View.VISIBLE);
            selectedViews[i].setVisibility(View.GONE);
        }
        selectedViews[0].setOnClickListener(this);
    }

    /**
     * Assign TAG on GUI
     */
    private void assignTags() {
        unselectedViews[0].performClick();
        currentSelectedTAG = selectedViews[0].getId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.announceView:
                callNextFragment(0);
                break;
            case R.id.announceViewSelected:
                if (currentSelectedTAG == -1) {
                    mActivity.onBackPressed();
                }
                break;
            case R.id.readingView:
                callNextFragment(1);
                break;
            case R.id.reflectionView:
                callNextFragment(2);
                break;
            case R.id.saintView:
                callNextFragment(3);
                break;
            case R.id.quizView:
                callNextFragment(4);
                break;
            case R.id.wordView:
                callNextFragment(5);
                break;
            case R.id.blogsView:
                callNextFragment(6);
                break;
            case R.id.forumsView:
                callNextFragment(7);
                break;
            case R.id.prayerView:
                callNextFragment(8);
                break;
            default:

                break;
        }
    }

    public void callNextFragment(int position) {
        if (position >= 0) {
            if (currentSelectedTAG != selectedViews[position].getId()) {
                manageViewVisibility(position);
                currentSelectedTAG = selectedViews[position].getId();
                MainActivity.getInstance().removeAllOldFragment();
                MainActivity.getInstance().showProgressBar(false);
                String current_date = getCurrentDate();
                switch (position) {
                    case 0:
                        SessionManager.getInstance(mActivity).setHomeDate(current_date);
                        MainActivity.getInstance().pushFragment(new DashboardFragment());
                        break;

                    case 1:
                        SessionManager.getInstance(mActivity).setTodaysReadingDate(current_date);
                        MainActivity.getInstance().pushFragment(new ReadingFragment());
                        break;
                    case 2:
                        SessionManager.getInstance(mActivity).setReflectionDate(current_date);
                        MainActivity.getInstance().pushFragment(new ReflectionFragment());
                        break;
                    case 3:
                        SessionManager.getInstance(mActivity).setSaintOFDayDate(current_date);
                        MainActivity.getInstance().pushFragment(new SaintOfDayFragment());
                        break;
                    case 4:
                        SessionManager.getInstance(mActivity).setQuizOFDayDate(current_date);
                        MainActivity.getInstance().pushFragment(new QuizOfDayFragment());
                        break;
                    case 5:
                        SessionManager.getInstance(mActivity).setWordOFDayDate(current_date);
                        MainActivity.getInstance().pushFragment(new WordOfDayFragment());
                        break;
                    case 6:
                        SessionManager.getInstance(mActivity).setActivitiesDate(current_date);
                        Intent intent = new Intent(MainActivity.getInstance(), DrawingLoadingScreen.class);
                        MainActivity.getInstance().startActivity(intent);
                        break;
                    case 7:
                        SessionManager.getInstance(mActivity).setTopicsDate(current_date);
                        MainActivity.getInstance().pushFragment(new TopicsFragment());
                        break;
                    case 8:
                        SessionManager.getInstance(mActivity).setPrayerDate(current_date);
                        MainActivity.getInstance().pushFragment(new PrayerFragment());
                        break;
                }
                CheckWhichTabIsToBlink();
            }
        } else {
            currentSelectedTAG = -1;
            MainActivity.getInstance().removeAllOldFragment();
            MainActivity.getInstance().showProgressBar(false);
            MainActivity.getInstance().pushFragment(new AnnounceFragment());
        }

    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
//        2015 - 12 - 30
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    public void callDashboard() {
//        currentSelectedTAG = 0;
//        MainActivity.getInstance().removeAllOldFragment();
//        MainActivity.getInstance().pushFragment(new DashboardFragment());
//        for (int index = 0; index < selectedViews.length; index++) {
//            selectedViews[index].setVisibility(View.GONE);
//            unselectedViews[index].setVisibility(View.VISIBLE);
//        }
        assignTags();

    }

    public void manageViewVisibility(int current) {
        for (int index = 0; index < selectedViews.length; index++) {
            if (index == current) {
                selectedViews[index].setVisibility(View.VISIBLE);
                unselectedViews[index].setVisibility(View.GONE);
            } else {
                selectedViews[index].setVisibility(View.GONE);
                unselectedViews[index].setVisibility(View.VISIBLE);
            }
        }
    }
}
