package com.biblezon.kidsdailyactivities.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.control.HeaderViewManager;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class BlogsFragment extends Fragment {

    private String TAG = BlogsFragment.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.blogs, container, false);
        initViews();
        manageHeaderOfScreen();
        return mView;
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                null);
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.blogs));
    }

}
