package com.biblezon.kidsdailyactivities.DrawingControl;

public class DrawActivitiesModel {

	String ImageId = "", imageTitle = "", imageDescription = "",
			imageThumb = "", imageUrl = "";
	String ImageName = ""/* , ThumbName = "" */;
	boolean isImageAlreadyDownload = true;

	public boolean isImageAlreadyDownload() {
		return isImageAlreadyDownload;
	}

	public void setImageAlreadyDownload(boolean isImageAlreadyDownload) {
		this.isImageAlreadyDownload = isImageAlreadyDownload;
	}

	public String getImageId() {
		return ImageId;
	}

	public void setImageId(String imageId) {
		ImageId = imageId;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getImageDescription() {
		return imageDescription;
	}

	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}

	public String getImageThumb() {
		return imageThumb;
	}

	public void setImageThumb(String imageThumb) {
		this.imageThumb = imageThumb;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageName() {
		return ImageName;
	}

	public void setImageName(String imageName) {
		ImageName = imageName;
	}

	// public String getThumbName() {
	// return ThumbName;
	// }
	//
	// public void setThumbName(String thumbName) {
	// ThumbName = thumbName;
	// }

}
