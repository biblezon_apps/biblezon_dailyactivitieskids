package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class AnnouncementBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = AnnouncementBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<ResponseBaseModel> mresponseBaseList = new ArrayList<ResponseBaseModel>();
    private String chosenDate="";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public AnnouncementBaseAPIHandler(Activity mActivity,
                                      WebAPIResponseListener webAPIResponseListener, String chosenDate) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        this.chosenDate=chosenDate;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.ANNOUNCEMENT+"&chosendate="+this.chosenDate).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response, true);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                loadResponseArray();
            }
        }) {
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.ANNOUNCEMENT_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response, boolean online) {
        if (response != null && WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
        /* Success of API Response */
            try {
                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
                for (int i = 0; i < mArrayData.length(); i++) {
                    JSONObject mOuterJsonObject = mArrayData.getJSONObject(i);
                    ResponseBaseModel mBaseModel = new ResponseBaseModel();
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_ID)) {
                        mBaseModel.setId(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_ID));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_TITLE)) {
                        mBaseModel.setTitle(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_TITLE));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_DESCRIPTION)) {
                        mBaseModel.setDescription(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_DESCRIPTION));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.DATE)) {
                        mBaseModel.setDate(mOuterJsonObject
                                .getString(GlobalKeys.DATE));
                    }
                    mresponseBaseList.add(mBaseModel);
                }

                if (online) {
                    saveResponseArray(mresponseBaseList);
                }
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        /* Fail of API Response */
            loadResponseArray();
        }

    }

    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ArrayList<ResponseBaseModel> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(GlobalKeys.ANNOUNCEMENT_KEY, json);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(GlobalKeys.ANNOUNCEMENT_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<ResponseBaseModel>>() {
            }.getType();
            mresponseBaseList = new ArrayList<ResponseBaseModel>();
            mresponseBaseList = gson.fromJson(json, type);

            if (mresponseBaseList != null && mresponseBaseList.size() > 0) {
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } else {
                mresponseBaseList = new ArrayList<ResponseBaseModel>();
                JSONObject jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, GlobalKeys.ANNOUNCEMENT_KEY + ".txt"));
                parseAPIResponse(jsonObject, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, GlobalKeys.ANNOUNCEMENT_KEY + ".txt"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            parseAPIResponse(jsonObject, false);
        }
    }
}