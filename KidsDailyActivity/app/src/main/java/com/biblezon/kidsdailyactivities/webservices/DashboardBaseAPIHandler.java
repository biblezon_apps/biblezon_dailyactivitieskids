package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.AppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class DashboardBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = DashboardBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<ResponseBaseModel> mresponseBaseList = new ArrayList<>();
    String API_KEY = "", API_URL = "", ID = "";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public DashboardBaseAPIHandler(Activity mActivity, String API_URL, String API_KEY, String ID,
                                   WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.API_KEY = API_KEY;
        this.API_URL = API_URL;
        this.ID = ID;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();
    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + API_URL+"&chosendate="+date).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response, true);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                loadResponseArray();
            }
        }) {
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                API_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response, boolean online) {
        if (response != null && WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
        /* Success of API Response */
            try {
//                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);

//                if (mArrayData != null && mArrayData.length() > 0) {

                JSONObject mDataJsonObject = response.getJSONObject(GlobalKeys.DATA);
                if (mDataJsonObject != null) {

                    /**
                     * for announcements
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_ANNOUNCEMENT)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_ANNOUNCEMENT);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_ANNOUNCEMENT, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_ANNOUNCEMENT);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }

                    /**
                     * for reading
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_READING)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_READING);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_READING, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_READING);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }

                    /**
                     * for reflection
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_REFLECTION)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_REFLECTION);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_REFLECTION, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_REFLECTION);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }

                    /**
                     * for saint of the day
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_SAINT_OF_DAY)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_SAINT_OF_DAY);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_SAINT_OF_DAY, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_SAINT_OF_DAY);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }
                    /**
                     * for quiz of the day
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_QUIZ_OF_DAY)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_QUIZ_OF_DAY);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_QUIZ_OF_DAY, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_QUIZ_OF_DAY);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }
                    /**
                     * for quiz of the day
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_WORD_OF_DAY)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_WORD_OF_DAY);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_WORD_OF_DAY, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_WORD_OF_DAY);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }
                    /**
                     * for discussion
                     */
                    if (mDataJsonObject.has(GlobalKeys.DASHBOARD_TOPICS)) {
                        JSONArray jsonArray = mDataJsonObject.getJSONArray(GlobalKeys.DASHBOARD_TOPICS);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            parseJsonObjectAddToList(GlobalKeys.DASHBOARD_TOPICS, jsonArray.getJSONObject(0));
                        } else {
                            ResponseBaseModel mBaseModel = new ResponseBaseModel();
                            mBaseModel.setId(GlobalKeys.DASHBOARD_TOPICS);
                            mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                            mresponseBaseList.add(mBaseModel);
                        }
                    }
                }

//                }

                for (ResponseBaseModel model : mresponseBaseList
                        ) {
                    AndroidAppUtils.showLog(TAG, "id in sequence : " + model.getId());
                }

                if (online) {
                    saveResponseArray(mresponseBaseList);
                }
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MainActivity.getInstance().showProgressBar(false);
            AppDialogUtils.showAlertDialog(mActivity, "Data Not Found.", "Ok", null);

        }
    }

    private void parseJsonObjectAddToList(String ID, JSONObject mOuterJsonObject) {
        try {
            ResponseBaseModel mBaseModel = new ResponseBaseModel();
            if (mOuterJsonObject.has(GlobalKeys.RESPONSE_ID)) {
                mBaseModel.setId(ID);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(new Date()); // Now use today date.
            String todaysDate = sdf.format(c.getTime());
            if (mOuterJsonObject.has(GlobalKeys.RESPONSE_TITLE)) {
                if (mOuterJsonObject
                        .getString(GlobalKeys.RESPONSE_TITLE) == null || mOuterJsonObject
                        .getString(GlobalKeys.RESPONSE_TITLE).length() == 0) {
                    mBaseModel.setTitle(mActivity.getResources().getString(R.string.nothing));
                } else {
                    mBaseModel.setTitle(mOuterJsonObject
                            .getString(GlobalKeys.RESPONSE_TITLE));
                }
            }
            if (mOuterJsonObject.has(GlobalKeys.RESPONSE_DESCRIPTION)) {
                AndroidAppUtils.showLog(TAG, "Here");
                mBaseModel.setDescription(mOuterJsonObject
                        .getString(GlobalKeys.RESPONSE_DESCRIPTION));
            }
            if (mOuterJsonObject.has(GlobalKeys.DATE)) {
                mBaseModel.setDate(mOuterJsonObject
                        .getString(GlobalKeys.DATE));
            }
            mresponseBaseList.add(mBaseModel);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Trip API Listener
     */
    private WebAPIResponseListener downloadImageResponse(final String image_name) {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
                if (arguments != null && arguments.length > 0
                        && arguments[0] != null
                        && !arguments[0].toString().isEmpty()) {
                    Bitmap bitmap = (Bitmap) arguments[0];
                    AndroidAppUtils.showLog(TAG, "bitmap : " + bitmap);
                    AppUtils.savePassengerBitmapToSDCard(bitmap, image_name);

                    // if (position == mMassBaseList.size()) {
//                    mResponseListener.onSuccessOfResponse(mresponseBaseList);
                    // }
                }

            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
//                AndroidAppUtils.hideProgressDialog();
            }
        };
        return mListener;
    }


    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ArrayList<ResponseBaseModel> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(API_KEY, json);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(API_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<ResponseBaseModel>>() {
            }.getType();
            mresponseBaseList = new ArrayList<ResponseBaseModel>();
            mresponseBaseList = gson.fromJson(json, type);

            if (mresponseBaseList != null) {
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } else {
                mresponseBaseList = new ArrayList<ResponseBaseModel>();
                JSONObject jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, API_KEY + ".txt"));
                parseAPIResponse(jsonObject, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, API_KEY + ".txt"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            parseAPIResponse(jsonObject, false);
        }
    }
}