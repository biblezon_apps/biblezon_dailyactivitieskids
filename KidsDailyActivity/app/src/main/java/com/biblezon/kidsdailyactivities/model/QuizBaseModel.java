package com.biblezon.kidsdailyactivities.model;

import java.util.ArrayList;

public class QuizBaseModel implements Comparable<QuizBaseModel> {

    String question, date;
    int score;
    String correct_answer;
    boolean already_answered, showscore;
    String id;
    ArrayList<QuizOptionBaseModel> options;

    public boolean isAlready_answered() {
        return already_answered;
    }

    public void setAlready_answered(boolean already_answered) {
        this.already_answered = already_answered;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isShowscore() {
        return showscore;
    }

    public void setShowscore(boolean showscore) {
        this.showscore = showscore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public ArrayList<QuizOptionBaseModel> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<QuizOptionBaseModel> options) {
        this.options = options;
    }

    @Override
    public int compareTo(QuizBaseModel another) {
        return another.getDate().compareTo(getDate());
    }
}
