package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.model.DiscussionBaseModel;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class GetDiscussionBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetDiscussionBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<DiscussionBaseModel> mresponseBaseList = new ArrayList<DiscussionBaseModel>();
    private String title, description, date;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetDiscussionBaseAPIHandler(Activity mActivity,
                                       WebAPIResponseListener webAPIResponseListener) {
//        AndroidAppUtils.showProgressDialog(mActivity, "Loading...", false);
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.DISCUSSION_KEY).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response);
//                        AndroidAppUtils.hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AppDialogUtils.showAlertDialog(mActivity, WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandlerReturningString(error, mActivity), "Ok", null);
                mResponseListener.onFailOfResponse();
//                AndroidAppUtils.hideProgressDialog();
//                loadResponseArray();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.GET_DISCUSSION_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response) {
        if (response != null && WebserviceResponseHandler.getInstance().checkPrayerRequestResponseCode(response)) {
        /* Success of API Response */
            try {
                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
                for (int i = 0; i < mArrayData.length(); i++) {
                    JSONObject mInnerJsonObject = mArrayData.getJSONObject(i);
                    DiscussionBaseModel mBaseModel = new DiscussionBaseModel();
                    if (mInnerJsonObject.has(GlobalKeys.PRAYER_REQUEST_MESSAGE)) {
                        mBaseModel.setComment(mInnerJsonObject
                                .getString(GlobalKeys.PRAYER_REQUEST_MESSAGE));
                    }
                    if (mInnerJsonObject.has(GlobalKeys.PRAYER_REQUEST_NAME)) {
                        mBaseModel.setFirst_name(mInnerJsonObject
                                .getString(GlobalKeys.PRAYER_REQUEST_NAME));
                    }
                    if (mInnerJsonObject.has(GlobalKeys.PRAYER_REQUEST_EMAIL)) {
                        mBaseModel.setEmail(mInnerJsonObject
                                .getString(GlobalKeys.PRAYER_REQUEST_EMAIL));
                    }
                    mresponseBaseList.add(mBaseModel);
                }
                saveResponseArray(mresponseBaseList);
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } catch (Exception e) {
                e.printStackTrace();
            }
//        } else {
        /* Fail of API Response */
//            loadResponseArray();
//        }

        }
//        else {
//        /* Fail of API Response */
//            loadResponseArray();
//        }
    }

    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ArrayList<DiscussionBaseModel> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(GlobalKeys.GET_DISCUSSION_KEY, json);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(GlobalKeys.GET_DISCUSSION_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<DiscussionBaseModel>>() {
            }.getType();

            mresponseBaseList = new ArrayList<DiscussionBaseModel>();
            mresponseBaseList = gson.fromJson(json, type);

            if (mresponseBaseList != null && mresponseBaseList.size() > 0) {
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } else {
                mresponseBaseList = new ArrayList<DiscussionBaseModel>();
                mResponseListener.onFailOfResponse();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
