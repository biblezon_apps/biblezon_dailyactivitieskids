package com.biblezon.kidsdailyactivities.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
    // make private static instance of Sessionmanager class
    private static SessionManager sessionManager;
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private Editor editor;
    // Context
    private Context mContext;

    // Constructor
    @SuppressLint("CommitPrefEdits")
    private SessionManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PreferenceHelper.PREFERENCE_NAME,
                PreferenceHelper.PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * getInstance method is used to initialize SessionManager singelton
     * instance
     *
     * @param context context instance
     * @return Singelton session manager instance
     */
    public static SessionManager getInstance(Context context) {
        if (sessionManager == null) {
            sessionManager = new SessionManager(context);
        }
        return sessionManager;
    }

    /**
     * Create login session
     */
    public void createLoginSession(String password, String email, String auth_token, String date) {
        // Storing login value as TRUE
        editor.putBoolean(PreferenceHelper.IS_LOGIN, true);
        // Storing User name and email in pref
        editor.putString(PreferenceHelper.KEY_USER_PASSWORD, password);
        editor.putString(PreferenceHelper.KEY_USER_EMAIL, email);
        editor.putString(PreferenceHelper.KEY_RATING_DATE, "");
        // Authorization Token
        editor.putString(PreferenceHelper.KEY_AUTHORIZATION_TOKEN, auth_token);
        // commit changes
        editor.commit();
    }

    /**
     * Get Login User name
     *
     * @return
     */
    public String getUserPassword() {
        return pref.getString(PreferenceHelper.KEY_USER_PASSWORD, null);
    }

    /**
     * Get Login User Email
     *
     * @return
     */
    public String getUserEmail() {
        return pref.getString(PreferenceHelper.KEY_USER_EMAIL, null);
    }


    public String getRatingDate() {
        return pref.getString(PreferenceHelper.KEY_RATING_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setRatingDate(String date) {
        editor.putString(PreferenceHelper.KEY_RATING_DATE, date);
        // commit changes
        editor.commit();
    }

    /**
     * Get authorization token
     *
     * @return
     */
    public String getAuthToken() {
        return (pref.getString(
                PreferenceHelper.KEY_AUTHORIZATION_TOKEN, null));
    }

    /**
     * Get token
     *
     * @return
     */
    public String getToken() {
        return (pref.getString(PreferenceHelper.KEY_AUTHORIZATION_TOKEN, null));
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(PreferenceHelper.IS_LOGIN, false);
    }


    public String getHomeDate() {
        return pref.getString(PreferenceHelper.KEY_HOME_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setHomeDate(String date) {
        editor.putString(PreferenceHelper.KEY_HOME_DATE, date);
        // commit changes
        editor.commit();
    }


    public String getTodaysReadingDate() {
        return pref.getString(PreferenceHelper.KEY_TODAYS_READING_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setTodaysReadingDate(String date) {
        editor.putString(PreferenceHelper.KEY_TODAYS_READING_DATE, date);
        // commit changes
        editor.commit();
    }


    /**
     * Set Rating date
     *
     * @param date
     */
    public void setReflectionDate(String date) {
        editor.putString(PreferenceHelper.KEY_REFLECTION_DATE, date);
        // commit changes
        editor.commit();
    }

    public String getReflectionDate() {
        return pref.getString(PreferenceHelper.KEY_REFLECTION_DATE, "");
    }


    public String getSaintOFDayDate() {
        return pref.getString(PreferenceHelper.KEY_SAINT_OF_DAY_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setSaintOFDayDate(String date) {
        editor.putString(PreferenceHelper.KEY_SAINT_OF_DAY_DATE, date);
        // commit changes
        editor.commit();
    }

    public String getQuizOFDayDate() {
        return pref.getString(PreferenceHelper.KEY_QUIZ_OF_DAY_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setQuizOFDayDate(String date) {
        editor.putString(PreferenceHelper.KEY_QUIZ_OF_DAY_DATE, date);
        // commit changes
        editor.commit();
    }

    public String getWordOFDayDate() {
        return pref.getString(PreferenceHelper.KEY_WORD_OF_DAY_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setWordOFDayDate(String date) {
        editor.putString(PreferenceHelper.KEY_WORD_OF_DAY_DATE, date);
        // commit changes
        editor.commit();
    }


    public String getActivitiesDate() {
        return pref.getString(PreferenceHelper.KEY_ACTIVITIES_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setActivitiesDate(String date) {
        editor.putString(PreferenceHelper.KEY_ACTIVITIES_DATE, date);
        // commit changes
        editor.commit();
    }

    public String getTopicsDate() {
        return pref.getString(PreferenceHelper.KEY_TOPICS_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setTopicsDate(String date) {
        editor.putString(PreferenceHelper.KEY_TOPICS_DATE, date);
        // commit changes
        editor.commit();
    }

    public String getPrayerDate() {
        return pref.getString(PreferenceHelper.KEY_TOPICS_DATE, "");
    }

    /**
     * Set Rating date
     *
     * @param date
     */
    public void setPrayerDate(String date) {
        editor.putString(PreferenceHelper.KEY_TOPICS_DATE, date);
        // commit changes
        editor.commit();
    }

}
