package com.biblezon.kidsdailyactivities.DrawingControl;

import java.io.File;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;


public class DrawingListAdapter extends
        RecyclerView.Adapter<DrawingListAdapter.ContactViewHolder> {

    private DrawingGridScreen activity;
    private String TAG = DrawingListAdapter.class.getSimpleName();

    public DrawingListAdapter(DrawingGridScreen context) {
        activity = context;
    }

    @Override
    public int getItemCount() {
        return ParseDrawApiResponse.mActivitiesModels.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder,
                                 final int i) {
        AndroidAppUtils.showLog(TAG, "POsition :" + i);
        String PATH = Environment.getExternalStorageDirectory() + "/"
                + GlobalKeys.STORE_IMAGE_FOLDER + "/"
                + ParseDrawApiResponse.mActivitiesModels.get(i).getImageName()
                + ".png";
        File file = new File(PATH);
        if (file.exists()) {
            Drawable drawable = BitmapDrawable.createFromPath(file
                    .getAbsolutePath());
            contactViewHolder.vImage.setImageDrawable(drawable);
        } else {
            AndroidAppUtils.showErrorLog(TAG, "Thumb not find.");
        }

        contactViewHolder.vText.setText(ParseDrawApiResponse.mActivitiesModels
                .get(i).getImageTitle());
        contactViewHolder.vImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String PATH = Environment.getExternalStorageDirectory()
                        + "/"
                        + GlobalKeys.STORE_IMAGE_FOLDER
                        + "/"
                        + ParseDrawApiResponse.mActivitiesModels.get(i)
                        .getImageName() + ".png";
                File file = new File(PATH);
                if (file.exists()) {
                    Intent intent = new Intent(activity, DrawMainDetailScreen.class);
                    intent.putExtra("key", i);
                    activity.startActivity(intent);
                } else
                    AndroidAppUtils.showToast(activity,
                            "This activity is dowloading..\nPlease try again.");

            }
        });


    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                int position) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.cardview, viewGroup, false);
        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected ImageView vImage;
        protected TextView vText;

        public ContactViewHolder(View v) {
            super(v);
            vImage = (ImageView) v.findViewById(R.id.icon_image);
            vText = (TextView) v.findViewById(R.id.icon_txt);
        }
    }
}
