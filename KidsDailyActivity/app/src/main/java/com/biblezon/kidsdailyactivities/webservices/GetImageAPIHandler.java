package com.biblezon.kidsdailyactivities.webservices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Environment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.DrawingControl.DrawActivitiesModel;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

/**
 * Get user image API Handler
 *
 * @author Shruti
 */
public class GetImageAPIHandler {
    /**
     * Instance object of get image API
     */
    @SuppressWarnings("unused")
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = GetImageAPIHandler.class.getSimpleName();
    /**
     * Request Data
     */
    private String image_path;
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private DrawActivitiesModel mDrawActivitiesModel;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public GetImageAPIHandler(Activity mActivity, String image_path,
                              WebAPIResponseListener webAPIResponseListener,
                              DrawActivitiesModel mDrawActivitiesModel) {

        this.mActivity = mActivity;
        this.mDrawActivitiesModel = mDrawActivitiesModel;
        this.image_path = image_path;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();
    }

    /**
     * Call API to Get User image
     */
    private void postAPICall() {
        AndroidAppUtils.showLog(TAG,
                "Now Download image is :" + image_path.trim());
        @SuppressWarnings("deprecation")
        ImageRequest mImageRequest = new ImageRequest(image_path.trim(),
                new Listener<Bitmap>() {

                    @Override
                    public void onResponse(Bitmap bitmap) {
                        if (bitmap != null) {
                            AndroidAppUtils.showLog(TAG,
                                    "Success To get Activity image");
                            storeDownloadImage(bitmap);
                            mDrawActivitiesModel.setImageAlreadyDownload(true);
                        } else
                            AndroidAppUtils.showErrorLog(TAG,
                                    "bitmap not downloaded.");
                        mResponseListener.onSuccessOfResponse();

                    }
                }, 0, 0, null, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AndroidAppUtils.showLog(TAG,
                        "get image onErrorResponse " + error);
                mResponseListener.onFailOfResponse();
            }
        }) {

        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToImageRequestQueue(
                mImageRequest, GetImageAPIHandler.class.getSimpleName());
        // set request time-out
        mImageRequest.setRetryPolicy(new DefaultRetryPolicy(
                1000 * 60,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    /**
     * Store Downloaded images
     *
     * @param mBitmap
     */
    private void storeDownloadImage(Bitmap mBitmap) {
        String PATH = Environment.getExternalStorageDirectory() + "/"
                + GlobalKeys.STORE_IMAGE_FOLDER + "/";
        File folder = new File(PATH);
        if (!folder.exists()) {
            folder.mkdir();// If there is no folder it will be created.
        }
        File file = new File(PATH, mDrawActivitiesModel.getImageName() + ".png");
        FileOutputStream outStream;
        try {
            outStream = new FileOutputStream(file);
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            AndroidAppUtils.showLog(TAG, "Image created success");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}