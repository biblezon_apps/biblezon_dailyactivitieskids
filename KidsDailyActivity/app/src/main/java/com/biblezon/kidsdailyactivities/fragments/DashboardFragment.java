package com.biblezon.kidsdailyactivities.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.adapter.DashboardListAdapter;
import com.biblezon.kidsdailyactivities.control.HeaderViewManager;
import com.biblezon.kidsdailyactivities.control.LeftSlidingMenuControl;
import com.biblezon.kidsdailyactivities.model.ResponseBaseModel;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.biblezon.kidsdailyactivities.webservices.DashboardBaseAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;

import java.util.ArrayList;


/**
 * Slider fragment Help Screen
 *
 * @author Anshuman
 */
public class DashboardFragment extends Fragment {

    /**
     * ResponseBaseModel Data Model
     */
    public static ResponseBaseModel mBaseModel;
    ListView todo_listview, suggestion_listview;
    TextView announcement, announcement_title;
    private String TAG = DashboardFragment.class.getSimpleName();
    private Activity mActivity;
    /**
     * Screen base view
     */
    private View mView;
    private DashboardListAdapter mTODOListAdapter, mSuggestionListAdapter;
    private ArrayList<ResponseBaseModel> mTODOArrayList, mSuggestionArrayList;

    /*
     * (non-Javadoc)
     *
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.dashboard, container, false);
        initViews();
        assignClicks();
        manageHeaderOfScreen();
        MainActivity.getInstance().showProgressBar(true);
        new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_KIDS, GlobalKeys.DASHBOARD_KIDS_KEY,
                mActivity.getResources().getString(R.string.announcements), AnnouncementApiResponseListener());
        return mView;
    }

    private void assignClicks() {
        announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LeftSlidingMenuControl.getInstance().callNextFragment(-1);
            }
        });
        announcement_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LeftSlidingMenuControl.getInstance().callNextFragment(-1);
            }
        });
    }

    /**
     * initializing view fields
     */
    private void initViews() {
        mActivity = getActivity();
        todo_listview = (ListView) mView.findViewById(R.id.todo_listview);
        suggestion_listview = (ListView) mView.findViewById(R.id.suggestion_listview);
        announcement = (TextView) mView.findViewById(R.id.announcement);
        announcement_title = (TextView) mView.findViewById(R.id.announcement_title);
        todo_listview.setCacheColorHint(Color.TRANSPARENT);
        todo_listview.requestFocus(0);
        mTODOArrayList = new ArrayList<>();
        mSuggestionArrayList = new ArrayList<>();
        mTODOListAdapter = new DashboardListAdapter(mActivity,
                mTODOArrayList);
        mSuggestionListAdapter = new DashboardListAdapter(mActivity, mSuggestionArrayList);
        todo_listview.setAdapter(mTODOListAdapter);
        suggestion_listview.setAdapter(mSuggestionListAdapter);
    }

    private WebAPIResponseListener AnnouncementApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ArrayList<ResponseBaseModel> responseList = (ArrayList<ResponseBaseModel>) arguments[0];
                    if (responseList != null && responseList.size() > 0) {
                        if (responseList.get(0).getId().equalsIgnoreCase(GlobalKeys.DASHBOARD_ANNOUNCEMENT)) {
                            announcement.setText(responseList.get(0).getTitle());
                        } else {
                            announcement.setText(mActivity.getResources().getString(R.string.nothing));
                        }

                        for (int i = 0; i < responseList.size(); i++) {
                            if (!(responseList.get(i).getId().
                                    equalsIgnoreCase(GlobalKeys.DASHBOARD_ANNOUNCEMENT)) && !(responseList.get(i).getId().
                                    equalsIgnoreCase(GlobalKeys.DASHBOARD_TOPICS))) {
                                mTODOArrayList.add(responseList.get(i));
                                if (mTODOArrayList != null
                                        && mTODOArrayList.size() > 0) {
                                    addDataIntoList();
                                }
                            }
                        }
                        if (responseList.get(responseList.size() - 1).getId().equalsIgnoreCase(GlobalKeys.DASHBOARD_TOPICS)) {
                            mSuggestionArrayList.add(responseList.get(responseList.size() - 1));
                            if (mSuggestionArrayList != null
                                    && mSuggestionArrayList.size() > 0) {
                                addSuggestionDataIntoList();
                            }
                        }

                    }


                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                MainActivity.getInstance().showProgressBar(false);
            }
        };
        return mListener;
    }

    private WebAPIResponseListener ReflectionApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel reflectionModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(reflectionModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_READING, GlobalKeys.DASHBOARD_READING_KEY,
                        mActivity.getResources().getString(R.string.todays_reading), ReadingApiResponseListener());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_READING, GlobalKeys.DASHBOARD_READING_KEY,
                        mActivity.getResources().getString(R.string.todays_reading), ReadingApiResponseListener());
            }
        };
        return mListener;
    }


    private WebAPIResponseListener ReadingApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel readingModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(readingModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_SAINT_OF_DAY, GlobalKeys.DASHBOARD_SAINT_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.saint_of_the_day), SaintApiResponseListener());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_SAINT_OF_DAY, GlobalKeys.DASHBOARD_SAINT_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.saint_of_the_day), SaintApiResponseListener());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener SaintApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel saintModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(saintModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_QUIZ_OF_DAY, GlobalKeys.DASHBOARD_QUIZ_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.quiz_of_the_day), QuizApiResponseListener());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_QUIZ_OF_DAY, GlobalKeys.DASHBOARD_QUIZ_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.quiz_of_the_day), QuizApiResponseListener());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener QuizApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel quizModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(quizModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_WORD_OF_DAY, GlobalKeys.DASHBOARD_WORD_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.word_of_the_day), WordApiResponseListener());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_WORD_OF_DAY, GlobalKeys.DASHBOARD_WORD_OF_DAY_KEY,
                        mActivity.getResources().getString(R.string.word_of_the_day), WordApiResponseListener());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener WordApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel prayerModel = (ResponseBaseModel) arguments[0];
                    mTODOArrayList.add(prayerModel);
                    if (mTODOArrayList != null
                            && mTODOArrayList.size() > 0) {
                        addDataIntoList();
                    }
                }
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_TOPICS, GlobalKeys.DASHBOARD_TOPICS_KEY,
                        mActivity.getResources().getString(R.string.topics), TopicsApiResponseListener());
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                new DashboardBaseAPIHandler(mActivity, GlobalKeys.DASHBOARD_TOPICS, GlobalKeys.DASHBOARD_TOPICS_KEY,
                        mActivity.getResources().getString(R.string.topics), TopicsApiResponseListener());
            }
        };
        return mListener;
    }

    private WebAPIResponseListener TopicsApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                if (arguments != null && arguments.length > 0 && arguments[0] != null) {
                    ResponseBaseModel discussionModel = (ResponseBaseModel) arguments[0];
                    mSuggestionArrayList.add(discussionModel);
                    if (mSuggestionArrayList != null
                            && mSuggestionArrayList.size() > 0) {
                        addSuggestionDataIntoList();
                    }
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
            }
        };
        return mListener;
    }

    private void addDataIntoList() {
        mTODOListAdapter.updateListData(mTODOArrayList);
        mTODOListAdapter.notifyDataSetChanged();
    }

    private void addSuggestionDataIntoList() {
        mSuggestionListAdapter.updateListData(mSuggestionArrayList);
        mSuggestionListAdapter.notifyDataSetChanged();
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                null);
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.dashboard));
    }


}
