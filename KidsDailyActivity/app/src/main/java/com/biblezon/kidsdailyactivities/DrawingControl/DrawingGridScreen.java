package com.biblezon.kidsdailyactivities.DrawingControl;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.biblezon.kidsdailyactivities.webservices.GetImageAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DrawingGridScreen extends Activity {

    public static Activity mActivity;
    private String TAG = DrawingGridScreen.class.getSimpleName();
    private DrawingListAdapter ca;
    RelativeLayout header_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawing_grid_screen);
        mActivity = this;
        RecyclerView recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);

        header_icon = (RelativeLayout) findViewById(R.id.back_rl);
        header_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        GridLayoutManager llm = new GridLayoutManager(DrawingGridScreen.this, 2);
        llm.setOrientation(GridLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);


        ca = new DrawingListAdapter(DrawingGridScreen.this);
        recList.setAdapter(ca);
        new loadImage().execute();
    }

    /**
     * Download new images
     */
    private void DownloadNewImages() {
        for (int i = 0; i < ParseDrawApiResponse.mActivitiesModels.size(); i++) {
            DrawActivitiesModel mDrawActivitiesModel = ParseDrawApiResponse.mActivitiesModels
                    .get(i);
            if (!AndroidAppUtils.checkIfImageAlreadyExists(mDrawActivitiesModel
                    .getImageName())) {
                new GetImageAPIHandler(mActivity,
                        mDrawActivitiesModel.getImageUrl(),
                        webAPIImageDowload(), mDrawActivitiesModel);
                return;
            }
        }
    }

    private WebAPIResponseListener webAPIImageDowload() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
                DownloadNewImages();
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                // TODO Auto-generated method stub

            }
        };
        return mListener;
    }

    class loadImage extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            CopyImageToSdCard();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            DownloadNewImages();
        }
    }

    /**
     * Copy Image TO Sd Card
     */
    private void CopyImageToSdCard() {
        for (int i = 0; i < GlobalKeys.DrawingImagesDrawableArray.length; i++) {
            copyImageFromResourceToSDCard(
                    GlobalKeys.DrawingImagesDrawableName[i],
                    GlobalKeys.DrawingImagesDrawableArray[i]);
        }
    }

    /**
     * Copy Image From Drawable to storage
     *
     * @param image_name
     */
    public void copyImageFromResourceToSDCard(String image_name, int ID) {
        String targetFileName = image_name + ".png";//
        if (!AndroidAppUtils.checkIfImageAlreadyExists(image_name)) {
            if (ID != 0) {
                Bitmap bm = BitmapFactory.decodeResource(getResources(), ID);

                String PATH = Environment.getExternalStorageDirectory() + "/"
                        + GlobalKeys.STORE_IMAGE_FOLDER + "/";
                File folder = new File(PATH);
                if (!folder.exists()) {
                    folder.mkdir();// If there is no folder it will be created.
                }
                File file = new File(PATH, targetFileName);
                FileOutputStream outStream;
                try {
                    outStream = new FileOutputStream(file);
                    bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                    outStream.flush();
                    outStream.close();
                    AndroidAppUtils.showLog(TAG, "Image created success");
                    mActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            if (ca != null) {
                                ca.notifyDataSetChanged();
                            }
                        }
                    });
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } else {
            AndroidAppUtils.showLog(TAG, "This Image is already into storage");
        }

    }


}
