package com.biblezon.kidsdailyactivities.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.DrawingControl.DrawMainDetailScreen;
import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.model.ActivityBaseModel;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppUtils;

import java.util.ArrayList;

public class ActivityListAdapter extends
        RecyclerView.Adapter<ActivityListAdapter.ContactViewHolder> {

    private ArrayList<ActivityBaseModel> responseList;
    private Activity activity;
    String TAG = ActivityListAdapter.class.getSimpleName();

    public ActivityListAdapter(Activity context) {
        activity = context;
        this.responseList = new ArrayList<ActivityBaseModel>();
    }

    public void addUpdatedData(ArrayList<ActivityBaseModel> responseArrayList) {
        this.responseList = new ArrayList<ActivityBaseModel>();
        responseList = responseArrayList;
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder,
                                 final int i) {
        // String PATH = Environment.getExternalStorageDirectory() + "/"
        // + GlobalKeys.USER_IMAGE_FOLDER_NAME + "/"
        // + responseList.get(i).getActivityThumb();
        // File file = new File(PATH);
        AndroidAppUtils.showLog(TAG, " thumb outer : "
                + responseList.get(i).getActivityThumb());
        if (AppUtils.checkIfImageAlreadyExists(responseList.get(i)
                .getActivityThumb())) {
            AndroidAppUtils.showLog(TAG, " thumb exist : "
                    + responseList.get(i).getActivityThumb());
            // Drawable drawable = BitmapDrawable.createFromPath(file
            // .getAbsolutePath());
            // contactViewHolder.vImage.setImageDrawable(drawable);

            contactViewHolder.vImage.setImageDrawable(new BitmapDrawable(
                    activity.getResources(), AppUtils
                    .getImageFromSDCard(responseList.get(i)
                            .getActivityThumb())));
        } else {
//			contactViewHolder.vImage.setImageDrawable(activity.getResources()
//					.getDrawable(
//							AndroidAppUtils.getId("thumb_"
//									+ responseList.get(i).getActivityID(),
//									R.drawable.class)));
//            contactViewHolder.vImage.setImageDrawable(activity.getResources()
//                    .getDrawable(R.drawable.login_logo));
            contactViewHolder.vImage.setVisibility(View.GONE);
        }
        contactViewHolder.vText.setText(responseList.get(i).getActivityTitle());
        contactViewHolder.vText.setVisibility(View.GONE);
        contactViewHolder.vImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DrawMainDetailScreen.class);
                if (AppUtils.checkIfImageAlreadyExists(responseList.get(i)
                        .getActivityImage())) {
                    intent.putExtra("image", responseList.get(i)
                            .getActivityImage());
                } else {
                    intent.putExtra("image", "image_"
                            + responseList.get(i).getActivityID());
                }
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                int position) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.cardview, viewGroup, false);
        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected ImageView vImage;
        protected TextView vText;

        public ContactViewHolder(View v) {
            super(v);
            vImage = (ImageView) v.findViewById(R.id.icon_image);
            vText = (TextView) v.findViewById(R.id.icon_txt);
        }
    }
}
