package com.biblezon.kidsdailyactivities.DrawingControl;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;

public class ParseDrawApiResponse {

	private String TAG = ParseDrawApiResponse.class.getSimpleName();
	public static ArrayList<DrawActivitiesModel> mActivitiesModels = new ArrayList<>();
	@SuppressWarnings("unused")
	private Activity mActivity = null;

	/**
	 * Default constructor of this class
	 */
	@SuppressLint("NewApi")
	public ParseDrawApiResponse(Activity mActivity, JSONObject mResponseJson,
			String mResponseStr) {
		this.mActivity = mActivity;
		try {
			if (mResponseJson == null && mResponseStr != null
					&& !mResponseStr.isEmpty()) {
				mResponseJson = new JSONObject(mResponseStr);
			}
			if (mResponseJson == null) {
				AndroidAppUtils.showErrorLog(TAG, "MResponse is null");
				return;
			}

			if (mResponseJson.has("replyCode")
					&& mResponseJson.getString("replyCode").equalsIgnoreCase(
							"success")) {
				if (mResponseJson.has("data")) {
					mActivitiesModels = new ArrayList<>();
					JSONArray mActivitiesArray = mResponseJson
							.getJSONArray("data");
					for (int i = 0; i < mActivitiesArray.length(); i++) {
						JSONObject mActivityDataJson = mActivitiesArray
								.getJSONObject(i);
						DrawActivitiesModel mDrawActivitiesModel = new DrawActivitiesModel();
						if (mActivityDataJson.has("id"))
							mDrawActivitiesModel.setImageId(mActivityDataJson
									.getString("id"));
						if (mActivityDataJson.has("title"))
							mDrawActivitiesModel
									.setImageTitle(mActivityDataJson
											.getString("title"));
						if (mActivityDataJson.has("description"))
							mDrawActivitiesModel
									.setImageDescription(mActivityDataJson
											.getString("description"));
						if (mActivityDataJson.has("thumb"))
							mDrawActivitiesModel
									.setImageThumb(mActivityDataJson
											.getString("thumb"));
						if (mActivityDataJson.has("image"))
							mDrawActivitiesModel.setImageUrl(mActivityDataJson
									.getString("image"));

						mDrawActivitiesModel.setImageName("image_"
								+ mDrawActivitiesModel.getImageId());
						// mDrawActivitiesModel.setThumbName("thumb_"
						// + mDrawActivitiesModel.getImageId());

						String imagename = "image_"
								+ mDrawActivitiesModel.getImageId();
						if (!AndroidAppUtils
								.checkIfImageAlreadyExists(imagename)) {
							mDrawActivitiesModel.setImageAlreadyDownload(false);
							AndroidAppUtils
									.showErrorLog(TAG,
											"This image Need to download :"
													+ imagename);
						} else
							AndroidAppUtils.showLog(TAG,
									"This image is already present into storage :"
											+ imagename);

						mActivitiesModels.add(mDrawActivitiesModel);
					}

				} else {
					AndroidAppUtils.showErrorLog(TAG, "data is not found");
				}
			} else {
				AndroidAppUtils.showErrorLog(TAG,
						"replyCode is not success or not found");
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
