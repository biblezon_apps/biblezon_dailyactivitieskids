package com.biblezon.kidsdailyactivities.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.iHelper.AlertDialogClickListener;
import com.biblezon.kidsdailyactivities.iHelper.ChoiceDialogClickListener;


public class AppDialogUtils {
    @SuppressWarnings("unused")
    private static String TAG = AppDialogUtils.class.getSimpleName();
    Activity mActivity;

    /**
     * Alert dialog to show message to user
     *
     * @param mActivity
     * @param msg
     * @param positiveButtonText
     */
    public static void showAlertDialog(Activity mActivity, String msg,
                                       String positiveButtonText,
                                       final AlertDialogClickListener mAlertDialogClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton(positiveButtonText,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (mAlertDialogClickListener != null) {
                            mAlertDialogClickListener
                                    .onClickOfAlertDialogPositive();
                        }
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Multiple User choice dialog
     *
     * @param mActivity
     * @param msg
     * @param positiveButtonText
     * @param navgitaveButtonText
     */
    public static void showChoiceDialog(Activity mActivity, String msg,
                                        String positiveButtonText, String navgitaveButtonText,
                                        final ChoiceDialogClickListener mChoiceDialogClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton(positiveButtonText,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mChoiceDialogClickListener.onClickOfPositive();
                    }
                });
        alertDialogBuilder.setNegativeButton(navgitaveButtonText,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        mChoiceDialogClickListener.onClickOfNegative();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Show Message Info View
     *
     * @param mActivity
     * @param msg
     */
    @SuppressLint({"InflateParams", "DefaultLocale"})
    public static void showMessageInfoWithOkButtonDialog(Activity mActivity, String heading,
                                                         String msg, int headingcolor, int msgcolor, final AlertDialogClickListener mAlertDialogClickListener) {
        /* Check if Alert is already there so remove old one */
        try {
            // Create custom dialog object
            if (mActivity != null) {
                final Dialog dialog = new Dialog(mActivity);
                // hide to default title for Dialog
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                // inflate the layout dialog_layout.xml and set it as
                // contentView
                LayoutInflater inflater = (LayoutInflater) mActivity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(
                        R.layout.show_message_button_dialog, null, false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setContentView(view);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

                // Retrieve views from the inflated dialog layout and update
                // their
                // values
                TextView messageText = (TextView) dialog
                        .findViewById(R.id.msgText);
                TextView headingText = (TextView) dialog
                        .findViewById(R.id.headingText);
                messageText.setText(msg);
                messageText.setTextColor(mActivity.getResources().getColor(msgcolor));
                headingText.setText(heading);
                headingText.setTextColor(mActivity.getResources().getColor(headingcolor));
                Button okButtontextDialog = (Button) dialog
                        .findViewById(R.id.okButtontextDialog);
                okButtontextDialog.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Dismiss the dialog
                        dialog.dismiss();
                        if (mAlertDialogClickListener != null) {
                            mAlertDialogClickListener
                                    .onClickOfAlertDialogPositive();
                        }

                    }
                });
                try {

                    // Display the dialog
                    dialog.show();
                } catch (BadTokenException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Show API response Dialog to user
     *
     * @param response
     */
    public static void showAPIResponseMessageDialog(Activity mActivity,
                                                    String response) {
        /* Response Status is null API Fail */
        // String errorMsg = WebserviceAPISuccessFailManager.getInstance()
        // .getReponseMessage(response);
        // errorMsg = errorMsg.replaceAll("_", " ");
        // errorMsg = StringUtils.capitalize(errorMsg);
        /* Response Status is null API Fail */
        // showMessageInfoWithOkButtonDialog(mActivity, errorMsg, null);
    }

}
