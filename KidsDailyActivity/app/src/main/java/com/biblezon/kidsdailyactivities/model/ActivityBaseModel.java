package com.biblezon.kidsdailyactivities.model;

public class ActivityBaseModel {

	String activityID, activityThumb, activityImage, activityDescription,
			activityTitle;

	public String getActivityID() {
		return activityID;
	}

	public String getActivityThumb() {
		return activityThumb;
	}

	public void setActivityThumb(String activityThumb) {
		this.activityThumb = activityThumb;
	}

	public void setActivityID(String activityID) {
		this.activityID = activityID;
	}

	public String getActivityImage() {
		return activityImage;
	}

	public void setActivityImage(String activityImage) {
		this.activityImage = activityImage;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getActivityTitle() {
		return activityTitle;
	}

	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}

}
