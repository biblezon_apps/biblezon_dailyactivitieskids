package com.biblezon.kidsdailyactivities;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ProgressBar;

import com.biblezon.kidsdailyactivities.control.HeaderViewManager;
import com.biblezon.kidsdailyactivities.control.LeftSlidingMenuControl;
import com.biblezon.kidsdailyactivities.fragments.DashboardFragment;
import com.biblezon.kidsdailyactivities.iHelper.ChoiceDialogClickListener;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.biblezon.kidsdailyactivities.webservices.VersionCheckAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;
import com.biblezon.kidsdailyactivities.webservices.WebserviceResponseHandler;

import org.json.JSONObject;

import java.util.Stack;

public class MainActivity extends FragmentActivity {


    public static FragmentActivity mActivity;
    /**
     * {@link MainActivity}
     */
    public static MainActivity mMainActivityInstance;
    /**
     * Debugging TAG
     */
    private String TAG = MainActivity.class.getSimpleName();
    /**
     * Fragment showing and opening Stack
     */
    private Stack<Fragment> mFragmentStack = new Stack<Fragment>();
    ProgressBar progressBar;
    DownloadManager downloadManager;
    private long downloadReference;

    /**
     * Instance of this class
     *
     * @return
     */
    public static MainActivity getInstance() {

        return mMainActivityInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = MainActivity.this;
        mMainActivityInstance = MainActivity.this;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mFragmentStack = new Stack<Fragment>();
        new LeftSlidingMenuControl(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new VersionCheckAPIHandler(mActivity, webAPIResponseLinsener());
            }
        }, 2000);
    }

    /**
     * Push Fragment into fragment Stack and show top fragment to GUI
     */
    public void pushFragment(Fragment mFragment) {
        try {
            // Begin the transaction
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();
            // add fragment on frame-layout
            transaction.add(R.id.fragment_container, mFragment);
            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commitAllowingStateLoss();
            // add fragment into flow stack
            mFragmentStack.add(mFragment);
            HeaderViewManager.mHeaderManagerInstance.changeRightButtonText(true);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /**
     * Pop Fragment from fragment stack and remove latest one and show Pervious
     * attached fragment
     */
    public void popFragment(Fragment mFragment) {
        try {
            // Begin the transaction
            FragmentTransaction ft = getSupportFragmentManager()
                    .beginTransaction();
            // remove fragment from view
            if (mFragment != null) {
                ft.remove(mFragment);
                ft.detach(mFragment);
            }
            ft.commit();
            getSupportFragmentManager().popBackStack();
            mFragmentStack.remove(mFragmentStack.lastElement());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mFragmentStack.size() == 1) {
            if (mFragmentStack.get(0) instanceof DashboardFragment) {
                Fragment mlastFm = mFragmentStack.lastElement();
                AppDialogUtils.showChoiceDialog(this, this.getResources()
                                .getString(R.string.exit_from_app), this.getResources()
                                .getString(R.string.yes),
                        this.getResources().getString(R.string.no),
                        AppExitManager());
            } else {
                LeftSlidingMenuControl.getInstance().callDashboard();
            }
        } else {

            super.onBackPressed();
            mFragmentStack.pop();
        }
    }

    public void showProgressBar(boolean show) {
        try {
            if (progressBar != null) {
                if (show)
                    progressBar.setVisibility(View.VISIBLE);
                else
                    progressBar.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Passenger Exit From App Process
     *
     * @return
     */
    private ChoiceDialogClickListener AppExitManager() {
        ChoiceDialogClickListener mClickListener = new ChoiceDialogClickListener() {

            @Override
            public void onClickOfPositive() {
                finish();
            }

            @Override
            public void onClickOfNegative() {

            }
        };

        return mClickListener;
    }

    /**
     * Remove all attached old fragments
     */
    public void removeAllOldFragment() {
        try {
            for (int i = 0; i < mFragmentStack.size(); i++) {
                popFragment(mFragmentStack.get(i));
            }
            mFragmentStack.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private WebAPIResponseListener webAPIResponseLinsener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
                try {
                    if (arguments.length > 0)
                        onAutoUpdateAPiResponse((JSONObject) arguments[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                // TODO Auto-generated method stub

            }
        };
        return mListener;
    }

    /**
     * on AutoUpdate API Response
     *
     * @param response
     */
    private void onAutoUpdateAPiResponse(JSONObject response) {
        if (WebserviceResponseHandler.getInstance().checkVersionResponseCode(
                response)) {
            /* Success of API Response */
            try {
                float latest_version = 0f;
                float currentVersion = 0f;
                try {
                    latest_version = Float.parseFloat(response.getString(GlobalKeys.LATEST_VERSION));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final String appURI = response
                        .getString(GlobalKeys.UPDATED_APP_URL);
                PackageInfo pInfo = null;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(
                            mActivity.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    currentVersion = Float.parseFloat(pInfo.versionName);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (latest_version > currentVersion) {
                    // oh yeah we do need an upgrade, let the user know send
                    // an alert message
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mActivity);
                    builder.setMessage(
                            "There is newer version of this application available, click OK to upgrade now?")
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        // if the user agrees to upgrade
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            // start downloading the file
                                            // using the download manager
                                            downloadManager = (DownloadManager) mActivity
                                                    .getSystemService(Context.DOWNLOAD_SERVICE);
                                            Uri Download_Uri = Uri
                                                    .parse(appURI);
                                            AndroidAppUtils.showLog(TAG, "appURI : " + appURI);
                                            DownloadManager.Request request = new DownloadManager.Request(
                                                    Download_Uri);
                                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                            request.setAllowedOverRoaming(false);
                                            request.setTitle("Daily Activities");
                                            request.setDestinationInExternalFilesDir(
                                                    mActivity,
                                                    Environment.DIRECTORY_DOWNLOADS,
                                                    GlobalKeys.APK_NAME
                                                            + ".apk");
                                            downloadReference = downloadManager
                                                    .enqueue(request);
                                        }
                                    });
                    // show the alert message
                    builder.create().show();
                } else {
                    // mResponseListener.onSuccessOfResponse();
                }

            } catch (Exception e) {
                e.printStackTrace();
                // mResponseListener.onSuccessOfResponse();
            }

        } else {
            /* Fail of API Response */
            // AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
            // "Something Went wrong !!", null);
            // mResponseListener.onSuccessOfResponse();
        }
    }

    // broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            AndroidAppUtils.showLog(TAG, "downloadReference *******  referenceId : " + downloadReference + " ******** " +
                    referenceId);
            if (downloadReference == referenceId) {

                AndroidAppUtils.showVerboseLog("VersionCheckAPIHandler",
                        "Downloading of the new app version complete");
                // start the installation of the latest version
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(downloadManager
                                .getUriForDownloadedFile(downloadReference),
                        "application/vnd.android.package-archive");
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(installIntent);

            }
        }
    };

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        IntentFilter filter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mActivity.registerReceiver(downloadReceiver, filter);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        try {
            if (downloadReceiver != null) {
                mActivity.unregisterReceiver(downloadReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
