package com.biblezon.kidsdailyactivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.control.HeaderViewManager;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.webservices.LoginAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.SignUpAPIHandler;
import com.biblezon.kidsdailyactivities.webservices.WebAPIResponseListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Shruti on 1/6/2016.
 */
public class SignUpActivity extends Activity {
    Activity mActivity;
    TextView submit_button;
    EditText email_et, name_et, password_et;
    String email, name, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_screen);
        initViews();
        assignClicks();
        manageHeaderOfScreen();
    }

    private void assignClicks() {
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidationBeforeLogin())
                    new SignUpAPIHandler(mActivity, name, email, password, ApiResponseListener());
            }
        });
    }

    private WebAPIResponseListener ApiResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
//                    SessionManager.getInstance(mActivity).createLoginSession(password, email, arguments[0].toString(), getCurrentDate());
//                startActivity(new Intent(SignUpActivity.this, MainActivity.class));
//                finish();
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
//                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
//                        mActivity.getResources().getString(R.string.try_again),
//                        "", R.color.black, R.color.orange, null);
            }
        };
        return mListener;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
//        2015 - 12 - 30
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    private void initViews() {
        mActivity = this;
        submit_button = (TextView) findViewById(R.id.submit_button);
        email_et = (EditText) findViewById(R.id.email_et);
        password_et = (EditText) findViewById(R.id.password_et);
        name_et = (EditText) findViewById(R.id.name_et);
    }

    /**
     * Check the validations on the view fields before performing login task
     */
    protected boolean checkValidationBeforeLogin() {
        if (email_et == null || name_et == null || password_et == null) {
            AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                    mActivity.getResources().getString(R.string.try_again),
                    "", R.color.black, R.color.orange, null);
            return false;
        } else {
            name = name_et.getText().toString();
            email = email_et.getText().toString();
            password = password_et.getText().toString();
            if (name == null || name.isEmpty()) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter your password.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else if (email == null || email.isEmpty()) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter your email id.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else if (!AndroidAppUtils.isEmailIDValidate(email)) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter valid email id.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else if (password == null || password.isEmpty()) {
                AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
                        "Please enter password.",
                        "", R.color.black, R.color.orange, null);
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * ManageHeader of the screen
     */
    private void manageHeaderOfScreen() {
        HeaderViewManager.getInstance().InitializeHeaderView(mActivity, null, false,
                null);
        HeaderViewManager.getInstance().setHeading(true,
                mActivity.getResources().getString(R.string.register));
    }

}
