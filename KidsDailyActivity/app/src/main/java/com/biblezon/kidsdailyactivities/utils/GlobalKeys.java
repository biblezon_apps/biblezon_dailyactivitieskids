package com.biblezon.kidsdailyactivities.utils;

import com.biblezon.kidsdailyactivities.R;

import java.util.Locale;

/**
 * Application Keys
 *
 * @author Anshuman
 */
public interface GlobalKeys {
    String BASE_URL = "http://biblezonadmin.com/biblezon/api/dailyactivity.php";/*"http://biblezon.com/appapicms/webservices/"*/
    String ImageFolder = "KidsDailyActivities";
    String ANNOUNCEMENT = "?action=getDailyReadings&category=ANNOUNCEMENT&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/1"*/
    String READING = "?action=getDailyReadings&category=READING&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/3"*/
    String REFLECTION = "?action=getDailyReadings&category=REFLECTION&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/2"*/
    String SAINT_OF_DAY = "?action=getDailyReadings&category=SAINT_OF_DAY&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/4"*/
    String WORD_OF_DAY = "?action=getDailyReadings&category=WORD_OF_DAY&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/6";*/
    String TOPICS = "?action=getDailyReadings&category=TOPICS&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"inbox/1/7";*/
    String QUIZ_OF_DAY = "?action=quizofday&devicetype=Kids&lang=" + Locale.getDefault().getDisplayLanguage();/*"quizofday/1";*/
    String PRAYER_REQUEST = "?action=getDailyReadings&category=PRAYER_LIST&lang=" + Locale.getDefault().getDisplayLanguage();/*"prayerrequest_list";*/
    String KIDS_LOGIN = "?action=adult_login&devicetype=Kids";/*"?action=kids_login";*/
    String KIDS_REGISTER = "?action=adult_register&devicetype=Kids";/*"?action=kids_register";*/
    String SUBMIT_QUIZ = "submit_quiz_of_day";
    String KidsDailyActivities = "KidsDailyActivities";
    String KIDS_DRAW_ACTIVITIES = "http://biblezonapps.com/appapicms/webservices/kidactivities";

    /* API Keys */
    String ANNOUNCEMENT_KEY = "announcement";
    String READING_KEY = "reading";
    String QUIZ_SCORE = "score";
    String QUIZ_SHOWSCORE = "showscore";
    String REFLECTION_KEY = "reflection";
    String SAINT_OF_DAY_KEY = "saint_of_day";
    String WORD_OF_DAY_KEY = "word_of_day";
    String QUIZ_OF_DAY_KEY = "quiz_of_day";
    String TOPICS_KEY = "topics";
    String LOGIN_KEY = "login";
    String REGISTER_KEY = "register";
    String ACTIVITY_KEY = "activities";
    String GET_IMAGE_KEY = "get_image";
    String SUBMIT_QUIZ_KEY = "submit_quiz";
    String DASHBOARD_KIDS = "?action=DASHBOARD_ADULT&lang=" + Locale.getDefault().getDisplayLanguage()/*"dashboard_kids"*/;
    String DASHBOARD_KIDS_KEY = "dashboard_kids_key";
    String DISCUSSION_KEY = BASE_URL + "prayerrequest_list";
    String GET_DISCUSSION_KEY = "get_discussion";
    String PRAYERREQUEST = BASE_URL + "prayerrequest";
    String PRAYERREQUEST_API_KEY = "prayerrequest_key";
    String PRAYERREQUEST_KEY = "prayer_key";

    /**
     * dashboard
     */
    String DASHBOARD_ANNOUNCEMENT = "Announcements";
    String DASHBOARD_READING = "Todays Readings";
    String DASHBOARD_REFLECTION = "Reflections";
    String DASHBOARD_SAINT_OF_DAY = "Saint of the day";
    String DASHBOARD_QUIZ_OF_DAY = "Quiz of the day";
    String DASHBOARD_WORD_OF_DAY = "Word Of the day";
    String DASHBOARD_TOPICS = "Topics";

    String DASHBOARD_ANNOUNCEMENT_KEY = "dashboard_announcement";
    String DASHBOARD_READING_KEY = "dashboard_reading";
    String DASHBOARD_REFLECTION_KEY = "dashboard_reflection";
    String DASHBOARD_SAINT_OF_DAY_KEY = "dashboard_saint_of_day";
    String DASHBOARD_QUIZ_OF_DAY_KEY = "dashboard_quiz_of_day";
    String DASHBOARD_WORD_OF_DAY_KEY = "dashboard_word_of_day";
    String DASHBOARD_TOPICS_KEY = "dashboard_topics";

    String RESPONSE_CODE = "replyCode";
    String RESPONSE_MESSAGE = "replyMsg";
    String SUCCESS = "SUCCESS";
    String DATA = "data";
    String TOKEN = "token";
    String DATE = "date";

    /* Response Activity Keys */
    String RESPONSE_ID = "id";
    String RESPONSE_TITLE = "title";
    String RESPONSE_DESCRIPTION = "description";
    String RESPONSE_IMAGE = "image";

    int ONE_SECOND = 1000;
    int API_TIME_OUT = 20;

    /**
     * version check keys
     */
    String VERSION_CHECK_KEY = "kidsdailyactivities";
    String MSG_SUCCESS = "success";
    String LATEST_VERSION = "latestVersion";
    String UPDATED_APP_URL = "appURI";
    String APK_NAME = "KidsDailyActivities";
    String AUTO_UPDATE_URL = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=com.biblezon.kidsdailyactivities&deviceId=";
    /**
     * quiz of the day keys
     */
    String QUIZ_QUESTION = "question";
    String QUIZ_OPTION_COUNT = "answer_id";
    String QUIZ_OPTION_TITLE = "option";
    String QUIZ_OPTIONS = "options";
    String QUIZ_QUIZ_ID = "quiz_of_day_id";
    String QUIZ_OPTION_CORRECTION = "is_correct";
    String QUIZ_CORRECT_ANSWER = "correct_answer";

    /**
     * Prayer request keys
     */
    String PRAYER_REQUEST_NAME = "name";
    String PRAYER_REQUEST_EMAIL = "email";
    String PRAYER_REQUEST_PASSWORD = "password";
    String PRAYER_REQUEST_MESSAGE = "message";
    /**
     * Submit quiz keys
     */
    String QUIZ_ID = "quiz_of_day_id";
    String IS_CORRECT = "is_correct";

    /* Mass Activity Keys */
    String MASS_ACTIVITY_ID = "id";
    String MASS_ACTIVITY_IMAGE = "image";
    String MASS_ACTIVITY_THUMB = "thumb";
    String MASS_ACTIVITY_DESCRIPTION = "description";
    String MASS_ACTIVITY_TITLE = "title";

    /**
     * Mass base Intent Key
     */
    String MASS_BASE_URL_KEY = "mass_url_key";
    String MASS_ACTIVITY_API = "mass_api";

    String STORE_IMAGE_FOLDER = "KidsActivitiesDrawing";
    // Increase it for big size and decrease it for make color box in small
    // size.
    int ColorBox_Size = 60;

    /**
     * Drawing Images
     */
    public static String[] DrawingImagesDrawableName = {"image_7",
            "image_7", "image_9", "image_10", "image_11", "image_12",
            "image_13", "image_14", "image_15", "image_16", "image_19",
            "image_20",
            "image_21",
            "image_22",
            "image_23",
            "image_24",
            "image_25",
            "image_26",
            "image_27",
            "image_28",
            "image_29",
            "image_30",
            "image_31",
            "image_32",
            "image_33",
            "image_34",
            "image_35",
            "image_36",
            "image_37",
            "image_38",
            "image_39",
            "image_40",
            "image_41",
            "image_42",
            "image_43",
            "image_44",
            "image_45",
            "image_46",
            "image_47",
            "image_48",
            "image_49",
            "image_50",
            "image_51",

    };

    /**
     * Drawing Images
     */
    public static int[] DrawingImagesDrawableArray = {R.drawable.image_20,
            R.drawable.image_21, R.drawable.image_22, R.drawable.image_23,
            R.drawable.image_24, R.drawable.image_25, R.drawable.image_26,
            R.drawable.image_27, R.drawable.image_28, R.drawable.image_29,
            R.drawable.image_30,
            R.drawable.image_31,
            R.drawable.image_32,
            R.drawable.image_33,
            R.drawable.image_34,
            R.drawable.image_35,
            R.drawable.image_36,
            R.drawable.image_37,
            R.drawable.image_38,
            R.drawable.image_40,
            R.drawable.image_41,
            R.drawable.image_42,
            R.drawable.image_43,
            R.drawable.image_44,
            R.drawable.image_45,
            R.drawable.image_46,
            R.drawable.image_47,
            R.drawable.image_48,
            R.drawable.image_48,
            R.drawable.image_50,
            R.drawable.image_51


    };

}
