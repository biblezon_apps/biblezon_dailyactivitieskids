package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.biblezon.kidsdailyactivities.LoginActivity;
import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.SignUpActivity;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.iHelper.AlertDialogClickListener;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Add name before ride API Handler
 *
 * @author Shruti
 */
public class SignUpAPIHandler {
    /**
     * Instance object of Add name after ride API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = SignUpAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    private String name = "", email = "", password = "";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public SignUpAPIHandler(Activity mActivity, String name, String email, String password,
                            WebAPIResponseListener webAPIResponseListener) {
        AndroidAppUtils.showProgressDialog(mActivity, "Loading...",
                false);
        this.mActivity = mActivity;
        this.name = name;
        this.email = email;
        this.password = password;
        AndroidAppUtils.showLog(TAG, "name :" + name + "\n email : " + email);
        this.mResponseListener = webAPIResponseListener;
        postAPICallString();
    }

    /**
     * Making String object request
     */
    public void postAPICallString() {
        String URL = (GlobalKeys.BASE_URL + GlobalKeys.KIDS_REGISTER).trim();
        AndroidAppUtils.showLog(TAG, "URL Post :" + URL);
        StringRequest strReq = new StringRequest(Request.Method.POST, URL
                , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
                AndroidAppUtils.hideProgressDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                AppDialogUtils.showAlertDialog(mActivity,
                        WebserviceAPIErrorHandler.getInstance()
                                .VolleyErrorHandlerReturningString(
                                        error, mActivity), "Ok", null);
                mResponseListener.onFailOfResponse();
                AndroidAppUtils.hideProgressDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.PRAYER_REQUEST_NAME, name);
                params.put(GlobalKeys.PRAYER_REQUEST_EMAIL, email);
                params.put(GlobalKeys.PRAYER_REQUEST_PASSWORD, password);
                return params;
            }
        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(strReq, GlobalKeys.REGISTER_KEY);
        // set request time-out
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                GlobalKeys.ONE_SECOND * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.CHANGE_KEY);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(String response) {
        // mResponseListener
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean status = WebserviceResponseHandler.getInstance()
                    .checkPrayerRequestResponseCode(jsonObject);
            if (status) {
            /* Response Success */
                mResponseListener.onSuccessOfResponse();
                AppDialogUtils.showAlertDialog(mActivity, WebserviceResponseHandler.getInstance().getResponseMessage(jsonObject),
                        "Ok", APIAlertDialogClickListener());

            } else {
            /* Response Status is null API Fail */
                mResponseListener.onFailOfResponse();
                AppDialogUtils.showAlertDialog(mActivity, WebserviceResponseHandler.getInstance().getResponseMessage(jsonObject),
                        "Ok", null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public AlertDialogClickListener APIAlertDialogClickListener() {
        AlertDialogClickListener alertDialogClickListener = new AlertDialogClickListener() {
            @Override
            public void onClickOfAlertDialogPositive() {
                mActivity.startActivity(new Intent(mActivity, LoginActivity.class));
                mActivity.finish();
            }
        };
        return alertDialogClickListener;
    }
}
