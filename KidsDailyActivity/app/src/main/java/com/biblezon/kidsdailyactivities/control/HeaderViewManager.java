package com.biblezon.kidsdailyactivities.control;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.kidsdailyactivities.R;
import com.biblezon.kidsdailyactivities.iHelper.HeaderViewClickListener;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;

import java.util.Calendar;


/**
 * Manage Header of the application
 *
 * @author Anshuman
 */
public class HeaderViewManager {

    /**
     * Instance of this class
     */
    public static HeaderViewManager mHeaderManagerInstance;
    /**
     * Debugging TAG
     */
    private String TAG = HeaderViewManager.class.getSimpleName();

    /**
     * Header View Instance
     */
    private RelativeLayout headerLeftView, headerRightView;
    private TextView headerHeadingText;
    private TextView headerLeftText, headerRightText;
    private ImageView headerLeftImage, headerRightImage;

    /**
     * Instance of Header View Manager
     *
     * @return
     */
    public static HeaderViewManager getInstance() {
        if (mHeaderManagerInstance == null) {
            mHeaderManagerInstance = new HeaderViewManager();
        }

        return mHeaderManagerInstance;
    }

    /**
     * Initialize Header View
     *
     * @param mActivity
     * @param mView
     * @param headerViewClickListener
     */
    public void InitializeHeaderView(Activity mActivity, View mView, boolean rightVisible,
                                     HeaderViewClickListener headerViewClickListener) {
        if (mActivity != null) {
            headerHeadingText = (TextView) mActivity
                    .findViewById(R.id.header_text);
            headerRightText = (TextView) mActivity
                    .findViewById(R.id.see_all);
        } else if (mView != null) {
            headerHeadingText = (TextView) mView
                    .findViewById(R.id.header_text);
            headerRightText = (TextView) mActivity
                    .findViewById(R.id.see_all);
        }
        if (rightVisible)
            headerRightText.setVisibility(View.VISIBLE);
        else
            headerRightText.setVisibility(View.GONE);
        if (headerViewClickListener != null)
            manageRightClickOnViews(headerViewClickListener);
//        manageClickOnViews(headerViewClickListener);
    }

    /**
     * ManageClickOn Header view
     *
     * @param headerViewClickListener
     */
    private void manageClickOnViews(
            final HeaderViewClickListener headerViewClickListener) {
        // Click on Header Left View
        headerLeftView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                headerViewClickListener.onClickOfHeaderLeftView();
            }
        });
        // Click on Header Right View
        headerRightView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                headerViewClickListener.onClickOfHeaderRightView();
            }
        });
    }

    /**
     * ManageClickOn Header view
     *
     * @param headerViewClickListener
     */
    private void manageRightClickOnViews(
            final HeaderViewClickListener headerViewClickListener) {
        // Click on Header Right View
        headerRightText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                headerViewClickListener.onClickOfHeaderRightView();
            }
        });
    }

    /**
     * Set Heading View Text
     *
     * @param isVisible
     * @param headingStr
     */
    public void setHeading(boolean isVisible, String headingStr) {
        if (headerHeadingText != null) {
            if (isVisible) {
                headerHeadingText.setVisibility(View.VISIBLE);
                headerHeadingText.setText(headingStr);
            } else {
                headerHeadingText.setVisibility(View.GONE);
            }
        } else {
            AndroidAppUtils.showErrorLog(TAG,
                    "Header Heading Text View is null");
        }
    }

    /**
     * Manage Header Left View
     *
     * @param isVisibleImage
     * @param isVisibleText
     * @param ImageId
     * @param LeftString
     */
    public void setLeftSideHeaderView(boolean isVisibleImage,
                                      boolean isVisibleText, int ImageId, String LeftString) {
        if (!isVisibleImage && !isVisibleText) {
            headerLeftView.setVisibility(View.GONE);
        } else if (headerLeftView == null || headerLeftText == null
                || headerLeftImage == null) {
            AndroidAppUtils.showErrorLog(TAG, "Header Left View is null");
        } else if (isVisibleImage) {
            headerLeftText.setVisibility(View.GONE);
            headerLeftImage.setVisibility(View.VISIBLE);
            if (ImageId > 0) {
                headerLeftImage.setImageResource(ImageId);
            } else {
                AndroidAppUtils.showErrorLog(TAG,
                        "Header left image id is null");
            }

        } else if (isVisibleText) {
            headerLeftText.setVisibility(View.VISIBLE);
            headerLeftImage.setVisibility(View.GONE);
            if (LeftString != null && !LeftString.isEmpty()) {
                headerLeftText.setText(LeftString);
            } else {
                AndroidAppUtils.showErrorLog(TAG,
                        "Header left header string is null");
            }
        }

    }

    /**
     * Set Header Right Side View
     *
     * @param isVisibleImage
     * @param isVisibleText
     * @param ImageId
     * @param RightString
     */
    public void setRightSideHeaderView(boolean isVisibleImage,
                                       boolean isVisibleText, int ImageId, String RightString) {
        if (!isVisibleImage && !isVisibleText) {
            headerRightView.setVisibility(View.GONE);
        } else if (headerRightView == null || headerRightText == null
                || headerRightImage == null) {
            AndroidAppUtils.showErrorLog(TAG, "Header Right View is null");
        } else if (isVisibleImage) {
            headerRightText.setVisibility(View.GONE);
            headerRightImage.setVisibility(View.VISIBLE);
            if (ImageId > 0) {
                headerRightImage.setImageResource(ImageId);
            } else {
                AndroidAppUtils.showErrorLog(TAG,
                        "Header Right image id is null");
            }

        } else if (isVisibleText) {
            headerRightText.setVisibility(View.VISIBLE);
            headerRightImage.setVisibility(View.GONE);
            if (RightString != null && !RightString.isEmpty()) {
                headerRightText.setText(RightString);
            } else {
                AndroidAppUtils.showErrorLog(TAG,
                        "Header Right header string is null");
            }
        }

    }



    public void changeRightButtonText(boolean single) {
      /*  if (single)
            headerRightText.setText("Choose Date");
        else
            headerRightText.setText("See Latest");
            */
        headerRightText.setText(Html.fromHtml("Oct 3<sup><small>rd</small></sup>"));
    }


    public void SetButtonText(String MonthDay, String suffix) {
      /*  if (single)
            headerRightText.setText("Choose Date");
        else
            headerRightText.setText("See Latest");
            */
        headerRightText.setText(Html.fromHtml(MonthDay+" <sup><small>"+ suffix +"</small></sup>"));
    }
    public  String theMonth(int month){
        String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        return monthNames[month];
    }
    public String getDayOfMonthSuffix(final int day)
    {
        String[] suffixes =
                //    0     1     2     3     4     5     6     7     8     9
                { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        //    10    11    12    13    14    15    16    17    18    19
                        "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                        //    20    21    22    23    24    25    26    27    28    29
                        "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        //    30    31
                        "th", "st" };
        return suffixes[day];
    }

    public String getChosenDate()
    {
        Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH);
        int dayOfMonth = now.get(Calendar.DAY_OF_MONTH);

        String MonthDay =   theMonth(month) + " " + dayOfMonth;
        String suffix= getDayOfMonthSuffix(dayOfMonth);
        SetButtonText(MonthDay,suffix );

        month =month+1;
        String ChosenDate=now.get(Calendar.YEAR) + "-" + month + "-" + now.get(Calendar.DAY_OF_MONTH);
        return ChosenDate;
    }
}
