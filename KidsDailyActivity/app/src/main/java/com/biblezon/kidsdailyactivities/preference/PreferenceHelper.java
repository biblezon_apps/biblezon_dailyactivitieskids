package com.biblezon.kidsdailyactivities.preference;

/**
 * Preference Helper Class
 *
 * @author Anshuman
 */
public interface PreferenceHelper {
    // Shared Preferences file name
    String PREFERENCE_NAME = "ADULT_DAILY_ACTIVITIES_PREFERENCE";
    // Shared Preferences mode
    int PRIVATE_MODE = 0;
    // User Id
    String KEY_USER_PASSWORD = "USER_PASSWORD";
    //User email id
    String KEY_USER_EMAIL = "USER_EMAIL";
    //Last Ratings Date
    String KEY_RATING_DATE = "rating_date";
    // authorization token Key
    String KEY_AUTHORIZATION_TOKEN = "AUTHORIZATION_TOKEN";
    // Login Shared Preferences Keys
    String IS_LOGIN = "IsLoggedIn";

    String KEY_HOME_DATE = "home_date";
    String KEY_TODAYS_READING_DATE = "todays_reading_date";
    String KEY_REFLECTION_DATE = "reflection_date";
    String KEY_SAINT_OF_DAY_DATE = "saint_of_day_date";
    String KEY_WORD_OF_DAY_DATE = "word_of_day_date";
    String KEY_QUIZ_OF_DAY_DATE = "quiz_of_day_date";
    String KEY_ACTIVITIES_DATE = "activities_date";
    String KEY_TOPICS_DATE = "topics_date";
    String KEY_PRAYER_DATE = "prayer_date";

}
