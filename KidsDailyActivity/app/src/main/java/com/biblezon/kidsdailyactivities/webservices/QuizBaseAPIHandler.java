package com.biblezon.kidsdailyactivities.webservices;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.kidsdailyactivities.MainActivity;
import com.biblezon.kidsdailyactivities.application.AppApplicationController;
import com.biblezon.kidsdailyactivities.model.QuizBaseModel;
import com.biblezon.kidsdailyactivities.model.QuizOptionBaseModel;
import com.biblezon.kidsdailyactivities.preference.SessionManager;
import com.biblezon.kidsdailyactivities.utils.AndroidAppUtils;
import com.biblezon.kidsdailyactivities.utils.AppDialogUtils;
import com.biblezon.kidsdailyactivities.utils.AppUtils;
import com.biblezon.kidsdailyactivities.utils.GlobalKeys;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * get commandments list Handler
 *
 * @author Shruti
 */
public class QuizBaseAPIHandler {
    /**
     * Instance object of get fav driver API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = QuizBaseAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * ArrayList Of Activities
     */
    private ArrayList<QuizBaseModel> mresponseBaseList = new ArrayList<QuizBaseModel>();
    private String chosenDate="";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public QuizBaseAPIHandler(Activity mActivity,
                              WebAPIResponseListener webAPIResponseListener, String mDate) {
        this.mActivity = mActivity;
        this.mResponseListener = webAPIResponseListener;
        this.chosenDate=mDate;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.QUIZ_OF_DAY+"&chosendate="+this.chosenDate).trim(),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AndroidAppUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response, true);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                loadResponseArray();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(GlobalKeys.TOKEN, SessionManager.getInstance(mActivity).getAuthToken());
                AndroidAppUtils.showLog(TAG, params.toString());
                return params;
            }
        };
        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.QUIZ_OF_DAY_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(GlobalKeys.ONE_SECOND
                * GlobalKeys.API_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response, boolean online) {
        if (response != null && WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
        /* Success of API Response */
            try {
                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
                for (int i = 0; i < mArrayData.length(); i++) {
                    ArrayList<QuizOptionBaseModel> modelArrayList = new ArrayList<>();
                    JSONObject mOuterJsonObject = mArrayData.getJSONObject(i);
                    QuizBaseModel mBaseModel = new QuizBaseModel();
                    if (mOuterJsonObject.has(GlobalKeys.QUIZ_CORRECT_ANSWER)) {
                        mBaseModel.setCorrect_answer(mOuterJsonObject
                                .getString(GlobalKeys.QUIZ_CORRECT_ANSWER));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.RESPONSE_ID)) {
                        mBaseModel.setId(mOuterJsonObject
                                .getString(GlobalKeys.RESPONSE_ID));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.DATE)) {
                        mBaseModel.setDate(mOuterJsonObject
                                .getString(GlobalKeys.DATE));
                    }
                    if (mOuterJsonObject.has(GlobalKeys.QUIZ_QUESTION)) {
                        mBaseModel.setQuestion(mOuterJsonObject
                                .getString(GlobalKeys.QUIZ_QUESTION));
                    }
                    /**
                     * score and showscore is in response part not data part
                     * but we are adding it in the model to save it into preference
                     */
                    if (response.has(GlobalKeys.QUIZ_SHOWSCORE)) {
                        mBaseModel.setShowscore(response
                                .getBoolean(GlobalKeys.QUIZ_SHOWSCORE));
                    }
                    if (response.has(GlobalKeys.QUIZ_SCORE)) {
                        mBaseModel.setScore(response
                                .getInt(GlobalKeys.QUIZ_SCORE));
                    }
                    JSONArray mOptionData = mOuterJsonObject.getJSONArray(GlobalKeys.QUIZ_OPTIONS);
                    for (int j = 0; j < mOptionData.length(); j++) {
                        QuizOptionBaseModel quizOptionBaseModel = new QuizOptionBaseModel();
                        JSONObject mInnerJsonObject = mOptionData.getJSONObject(j);
                        if (mInnerJsonObject.has(GlobalKeys.QUIZ_QUIZ_ID)) {
                            quizOptionBaseModel.setQuiz_id(mInnerJsonObject
                                    .getString(GlobalKeys.QUIZ_QUIZ_ID));
                        }
                        if (mInnerJsonObject.has(GlobalKeys.QUIZ_OPTION_CORRECTION)) {
                            quizOptionBaseModel.setCorrect(mInnerJsonObject
                                    .getString(GlobalKeys.QUIZ_OPTION_CORRECTION));
                        }
                        if (mInnerJsonObject.has(GlobalKeys.QUIZ_OPTION_TITLE)) {
                            quizOptionBaseModel.setOption(mInnerJsonObject
                                    .getString(GlobalKeys.QUIZ_OPTION_TITLE));
                        }
                        if (mInnerJsonObject.has(GlobalKeys.QUIZ_OPTION_COUNT)) {
                            quizOptionBaseModel.setOption_count(mInnerJsonObject
                                    .getString(GlobalKeys.QUIZ_OPTION_COUNT));
                        }
                        modelArrayList.add(quizOptionBaseModel);
                    }
                    mBaseModel.setOptions(modelArrayList);
                    mresponseBaseList.add(mBaseModel);
                }
                if (online) {
                    saveResponseArray(mresponseBaseList);
                }
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            MainActivity.getInstance().showProgressBar(false);
            AppDialogUtils.showAlertDialog(mActivity, "Data Not Found.", "Ok", null);
        }
    }

    /**
     * Save Notes Data into shared Preferences
     *
     * @param mbitArray
     * @return
     */
    private boolean saveResponseArray(ArrayList<QuizBaseModel> mbitArray) {
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = gson.toJson(mbitArray);
            prefsEditor.putString(GlobalKeys.QUIZ_OF_DAY_KEY, json);
            return prefsEditor.commit();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restore List data from shared Preferences
     */
    @SuppressWarnings("unused")
    private void loadResponseArray() {
        AndroidAppUtils.showLog(TAG, "loadResponseArray");
        try {
            SharedPreferences appSharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(mActivity);
            SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
            Gson gson = new Gson();
            String json = appSharedPrefs.getString(GlobalKeys.QUIZ_OF_DAY_KEY, "");
            java.lang.reflect.Type type = new TypeToken<ArrayList<QuizBaseModel>>() {
            }.getType();
            mresponseBaseList = new ArrayList<QuizBaseModel>();
            mresponseBaseList = gson.fromJson(json, type);

            if (mresponseBaseList != null && mresponseBaseList.size() > 0) {
                mResponseListener.onSuccessOfResponse(mresponseBaseList);
            } else {
                mresponseBaseList = new ArrayList<QuizBaseModel>();
                JSONObject jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, GlobalKeys.QUIZ_OF_DAY_KEY + ".txt"));
                parseAPIResponse(jsonObject, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(AppUtils
                        .LoadData(mActivity, GlobalKeys.QUIZ_OF_DAY_KEY + ".txt"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            parseAPIResponse(jsonObject, false);
        }
    }
}